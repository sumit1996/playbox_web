<?php

use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\FriendController;
use App\Http\Controllers\MainController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Route::post('register', 'API\RegisterController@register');
//Route::post('login', 'API\RegisterController@login');

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

//Route::group([
//    'prefix' => 'auth'
//], function () {
//    Route::post('register', 'Auth\RegisterController@register');
//
//    Route::group([
//        'middleware' => 'auth:api'
//    ], function () {
//
//
//    });
//
//});
//Route::post('register', [App\Http\Controllers\Auth\RegisterController::class, 'register']);
//Route::post('login', [App\Http\Controllers\Auth\RegisterController::class, 'login']);
Route::get('/demo-url',  function  (Request $request)  {
    return response()->json(['Laravel 8 CORS Demo']);
});
Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('register', [RegisterController::class, 'register']);
    Route::post('reset_password', [MainController::class, 'reset_password']);
    Route::post('login', [RegisterController::class, 'login']);
    Route::get('all-challenges', [MainController::class, 'all_challenges']);
    Route::get('check-fb-id/{id}', [MainController::class, 'checkFbId']);
    Route::get('check-google-id/{id}', [MainController::class, 'checkGoogleId']);
    Route::group([
        'middleware' => 'auth:api'
//        'middleware' => ['cors', 'auth.api']
    ], function () {
//        Route::get('user', [MainController::class, 'user_info']);
        Route::post('get_challenge_data', [MainController::class, 'get_challenge_data']);
        Route::post('get_all_entry', [MainController::class, 'get_all_entry']);
        Route::post('game_entry_data', [MainController::class, 'game_entry_data']);
        Route::post('get_all_group_entry', [MainController::class, 'get_all_group_entry']);
        Route::post('game_entry_like', [MainController::class, 'game_entry_like']);
        Route::post('count_likes', [MainController::class, 'count_likes']);
        Route::get('logout', [RegisterController::class, 'logout']);

        Route::post('save_prediction_data', [MainController::class, 'save_prediction_data']);
        Route::get('user-data', [MainController::class, 'userInformation']);
        Route::post('update_profile', [MainController::class, 'update_profile']);
        Route::post('user_image_save', [MainController::class, 'user_image_save']);
        Route::post('save_my_game', [MainController::class, 'save_my_game']);
        Route::post('get_my_game', [MainController::class, 'get_my_game']);
        Route::post('remove_my_game', [MainController::class, 'remove_my_game']);
        Route::post('game_entry_report', [MainController::class, 'game_entry_report']);

        Route::post('game_entry_comment', [MainController::class, 'game_entry_comment']);
        Route::post('gat_all_comment', [MainController::class, 'gat_all_comment']);
        Route::post('all-users', [MainController::class, 'all_users']);
        Route::post('get_friends', [MainController::class, 'get_friends']);

        Route::post('friend_reuest', [FriendController::class, 'friend_reuest']);
        Route::post('get_friend_request', [FriendController::class, 'get_friend_request']);
        Route::post('get_send_friend_request', [FriendController::class, 'get_send_friend_request']);
        Route::post('accept_friend_request', [FriendController::class, 'accept_friend_request']);

        Route::post('gat_all_prediction', [MainController::class, 'gat_all_prediction']);
        Route::post('get_points', [MainController::class, 'get_points']);

        Route::post('all-world-ranking', [MainController::class, 'all_world_ranking']);

        Route::post('save_onesignal_player_id', [MainController::class, 'save_onesignal_player_id']);

        Route::post('get_video_challenge_data', [MainController::class, 'get_video_challenge_data']);
        Route::post('get_all_video_entry', [MainController::class, 'get_all_video_entry']);


        Route::post('get_all_group_entry_video', [MainController::class, 'get_all_group_entry_video']);
        Route::post('get_user_percentage_ranking', [MainController::class, 'get_user_percentage_ranking']);

        Route::post('get_user_friend_played_game', [MainController::class, 'get_user_friend_played_game']);
        Route::post('get_like_count_user_like_data', [MainController::class, 'get_like_count_user_like_data']);

        Route::post('add_challenge_from_user', [MainController::class, 'add_challenge_from_user']);

    });
//    Route::middleware('auth:api')->get('/user', function (Request $request) {
//        return new JsonResponse([
//            'user_details' => $request->user()
//        ]);
//    });
//    Route::get('/user', function(Request $request) {
//        return Auth::user();
//    })->middleware('auth:api');
});

