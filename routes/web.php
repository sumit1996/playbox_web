<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});
Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
//Auth::routes();
Route::post('/video', 'App\Http\Controllers\MainController@save_video')->name('save_video');
Route::get('/add-video/{challenge_id}/{user_id}/{points}', 'App\Http\Controllers\MainController@add_video')->name('add_video');

Route::post('/saveChallenge', 'App\Http\Controllers\MainController@add_challenge_from_user')->name('add_challenge_from_user');
Route::get('/add-challenge/{user_id}', 'App\Http\Controllers\MainController@add_challenge')->name('add_challenge');


Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('home')->middleware('auth');

Route::group(['middleware' => 'auth','prefix' => 'admin',], function () {
    // start challenge
    Route::get('/challenge', [App\Http\Controllers\ChallengeController::class, 'show'])->name('challenge.show');
    Route::get('/challenge/add', [App\Http\Controllers\ChallengeController::class, 'add'])->name('challenge.add');
    Route::get('/challenge/edit/{id}', [App\Http\Controllers\ChallengeController::class, 'edit'])->name('challenge.edit');
    Route::post('/challenge/update', [App\Http\Controllers\ChallengeController::class, 'update'])->name('challenge.update');
    Route::get('/challenge/delete/{id}', [App\Http\Controllers\ChallengeController::class, 'delete'])->name('challenge.delete');
    Route::get('/challenge/view/{id}', [App\Http\Controllers\ChallengeController::class, 'view'])->name('challenge.view');

    Route::get('/prediction', [App\Http\Controllers\ChallengeController::class, 'prediction'])->name('prediction');
    Route::get('/prediction/edit/{id}', [App\Http\Controllers\ChallengeController::class, 'prediction_edit'])->name('prediction.edit');
    Route::post('/prediction/update', [App\Http\Controllers\ChallengeController::class, 'prediction_update'])->name('prediction.update');
});

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);
});

