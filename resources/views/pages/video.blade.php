<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://http://code.jquery.com/jquery-1.7.1.min.js"></script>
    <title>Video Upload</title>
    <style>
        .container {
            max-width: 500px;
        }

        dl, ol, ul {
            margin: 0;
            padding: 0;
            list-style: none;
        }
        .loader {
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid blue;
            border-right: 16px solid green;
            border-bottom: 16px solid red;
            border-left: 16px solid pink;
            width: 120px;
            height: 120px;
            -webkit-animation: spin 2s linear infinite;
            animation: spin 2s linear infinite;
        }

        @-webkit-keyframes spin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
        .hide {
            display: none;
        }
    </style>
</head>

<body>
{{--{{$response}}--}}
<div class="container mt-5">
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <strong>{{ $message }}</strong>
        </div>
        @else
        <form action="{{route('save_video')}}" method="post" enctype="multipart/form-data">
            <h3 class="text-center mb-5">Upload Video</h3>

            <div class="loader hide"></div>
            @csrf

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <input type="hidden" name="challege_id" value="{{$challenge_id}}">
            <input type="hidden" name="user_id" value="{{$user_id}}">
            <input type="hidden" name="points" value="{{$points}}">
            <textarea name="description" placeholder="Description" style="height: 100px;
    width: 100%;
    padding: 6px 15px;
    background: #f3f3f5;
    outline: none;
    margin-bottom: 10px;
    border: 1px solid #ccc;
    border-radius: 4px;"></textarea>
            <div class="custom-file">
                <input type="file" name="thumbnail" id="thumbnail-upload" class="custom-file-input">
                <label class="custom-file-label" id="thumbnail-uploads" for="thumbnail-upload">Add Thumbnail Image</label>
            </div>
            <br>
            <div class="custom-file">
                <input type="file" name="file" id="file-upload" class="custom-file-input">
                <label class="custom-file-label" id="file-uploads" for="file-upload">Select file</label>
            </div>


            <button type="submit" name="submit" id="videoform" class="btn btn-upload" style="display: block;
    margin: 0 auto;
    padding: 10px 30px;
    text-transform: uppercase;
    font-size: 15px;
    font-weight: bold;
    border-radius: 22px;
    min-width: 150px;
    color: #fff;
    background: #28ba62;
    margin-top: 10px;
    margin-bottom: 10px;">
                Upload Files
            </button>

        </form>
    @endif

</div>

</body>

@if ($message = Session::get('success'))
    <script>
                $(".loader").addClass('hide');
    </script>
@endif
<script>
    $(document).ready(function(){
        $("#videoform").on("click", function(){
            // alert('ye ayay');
            $(".loader").removeClass('hide');
        });//submit

        $('#file-upload').change(function() {
            var i = $(".custom-file-label").clone();
            var file = $('#file-upload')[0].files[0].name;
            $("#file-uploads").text(file);
        });
        $('#thumbnail-upload').change(function() {
            var i = $(".custom-file-label").clone();
            var file1 = $('#thumbnail-upload')[0].files[0].name;
            $("#thumbnail-uploads").text(file1);
        });

    });//document ready
</script>

</html>
