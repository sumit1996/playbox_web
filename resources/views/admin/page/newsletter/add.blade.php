@extends('admin.layouts.app')

@section('content')
    <div class="col-md-12 center-form">
        <div class="card">
            <form method="POST" action="{{route('admin.newsletter.update')}}" class="form-horizontal"
                  enctype="multipart/form-data">
                @csrf
                <div class="card-header card-header-info">
                    <h4 class="card-title">Add New Newsletter Subscription</h4>
                </div>
                <div class="card-body">

                    <div class="row">
                        <label class="col-sm-2 control-lbl">
                            Email :
                        </label>
                        <div class="col-md-10">
                            <div class="form-group bmd-form-group is-filled">
                                <input type="text"
                                       class="form-control @error('email') is-invalid @enderror"
                                       name="email"
                                       value="{{old('email')}}"
                                       required>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-2 control-lbl">
                            Site Name :
                        </label>
                        <div class="col-md-10">
                            <div class="form-group bmd-form-group is-filled">
                                <select name="site_id" id="site_id"
                                        class="change-user form-control @error('site_id') is-invalid @enderror">
                                    <option value="">------- Select Site -------</option>
                                    @foreach (\App\Site::all() as $site)
                                        <option value="{{ $site->id }}"
                                                @if(old('site_id')==$site->title ) selected @endif>{{ $site->title }}</option>
                                    @endforeach
                                </select>
                                @error('site_id')
                                <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-footer ml-auto text-right">
                    <a href="{{route('admin.sliders.show')}}" class="btn btn-secondary"
                       data-dismiss="modal">{{ __('Cancel') }}</a>
                    <button type="submit" class="btn btn-info">
                        Add
                    </button>
                </div>
            </form>
        </div>
    </div>

@endsection

