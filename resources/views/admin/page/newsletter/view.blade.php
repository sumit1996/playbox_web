@extends('admin.layouts.app')

@section('content')

    <div class="col-md-12 center-form">
        <div class="card">

            <div class="card-header card-header-info">
                <h4 class="card-title">View Newsletter Subscription</h4>
            </div>
            <div class="card-body">
                <div class="row">
                    <label class="col-sm-2 " style="color:black;">
                        <h4><strong>ID : {{$newsletter->id}}</strong></h4>
                    </label>
                </div>
                <div class="row">
                    <label class="col-sm-2 control-lbl">
                        Email :
                    </label>
                    <div class="col-md-10">
                        <div class="form-group bmd-form-group is-filled">
                            {{ $newsletter->email }}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-2 control-lbl">
                        Site name :
                    </label>
                    <div class="col-md-10">
                        <div class="form-group bmd-form-group is-filled">
                            {{\App\Helpers\Helper::get_site_name($newsletter->site_id)}}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-2 control-lbl">
                        Created at :
                    </label>
                    <div class="col-md-10">
                        <div class="form-group bmd-form-group is-filled">
                            {{$newsletter->created_at}}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-2 control-lbl">
                        Updated at :
                    </label>
                    <div class="col-md-10">
                        <div class="form-group bmd-form-group is-filled">
                            @if($newsletter->updated_at=='') Null @else {{$newsletter->updated_at}} @endif
                        </div>
                    </div>
                </div>

            </div>
            <div class="card-footer ml-auto text-right">
                <a href="{{route('admin.newsletter.show')}}" class="btn btn-info"
                   data-dismiss="modal">{{ __('Cancel') }}</a>
            </div>
        </div>
    </div>

@endsection


