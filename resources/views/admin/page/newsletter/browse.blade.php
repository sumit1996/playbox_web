@extends('admin.layouts.app')

@section('content')

    <div class="addresses-page full-width">
        <div class="col-md-12">
            <!-- Button trigger modal -->
            <a href="{{route('admin.newsletter.add')}}" type="button" class="btn btn-info">
                Add Newsletter Subscription <i class="material-icons">add</i>
            </a>
            <br>
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title ">All {{ $status }}</h4>
                    <p class="card-category">Here is all current {{ $status }}.</p>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table user-table">
                            <thead class=" text-primary">
                            <tr>
                                <th>
                                    S.NO
                                </th>
                                <th>
                                    Email
                                </th>
                                <th>
                                    Site name
                                </th>
                                <th>
                                    Created at
                                </th>
                                <th>
                                    Actions
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!$newsletter->isEmpty())
                                @foreach($newsletter as $key => $item)
                                    <tr>
                                        <td>
                                            {{$no++}}
                                        </td>
                                        <td>
                                            {{$item->email}}
                                        </td>
                                        <td>
                                            {{\App\Helpers\Helper::get_site_name($item->site_id)}}
                                        </td>
                                        <td>
                                            {{$item->created_at}}
                                        </td>
                                        <td class="text-primary action-edit">
                                            <a href="{{route('admin.newsletter.edit', ['id' => $item->id]) }}"
                                               class="btn btn-success btn-link "><i
                                                    class="material-icons">edit</i></a> <!--edit-user-btn -->
                                            |
                                            <a href="{{route('admin.newsletter.view', ['id' => $item->id]) }}"
                                               class="btn btn-info btn-link view-user-btn">
                                                <i class="material-icons">remove_red_eye</i>
                                            </a>
                                            |
                                            <a href="#deleteuser"
                                               data-userid="{{route('admin.newsletter.delete', ['id' => $item->id]) }}"
                                               data-toggle="modal" class="btn btn-danger btn-link delete-user-btn"><i
                                                    class="material-icons">delete</i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td>
                                        Any delivery option does not exist.
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- Delete Modal -->
        @include('admin.page.delete_confirmation_popup', ['title' => 'newsletter subscription'])
    </div>

@endsection
