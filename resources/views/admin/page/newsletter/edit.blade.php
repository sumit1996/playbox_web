@extends('admin.layouts.app')

@section('content')
    @if($newsletter!=null)
        <div class="col-md-12 center-form">
            <div class="card">
                <form method="POST" action="{{route('admin.newsletter.update')}}" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="card-header card-header-info">
                        <h4 class="card-title">Edit Newsletter Subscription</h4>
                    </div>
                    <div class="card-body">
                        <input type="hidden" name="id" value="{{$newsletter->id}}">

                        <div class="row">
                            <label class="col-sm-2 control-lbl">
                                Email :
                            </label>
                            <div class="col-md-10">
                                <div class="form-group bmd-form-group is-filled">
                                    <input type="text"
                                           class="form-control @error('email') is-invalid @enderror"
                                           name="email"
                                           value="@if(old('email')){{old('email')}}@else{{$newsletter->email}}@endif"
                                           required>

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-sm-2 control-lbl">
                                Site Title :
                            </label>
                            <div class="col-md-10">
                                <div class="form-group bmd-form-group is-filled">
                                    <select name="site_id" class="form-control @error('site_id') is-invalid @enderror">
                                        @foreach (\App\Site::get() as $site)
                                            <option value="{{ $site->id }}"
                                                    @if(old('site_id')==$site->id || $newsletter->site_id == $site->id) selected @endif>{{ $site->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer ml-auto text-right">
                        <a href="{{route('admin.newsletter.show')}}" class="btn btn-secondary"
                           data-dismiss="modal">{{ __('Cancel') }}</a>
                        <button type="submit" class="btn btn-info">
                            Update
                        </button>
                    </div>
                </form>
            </div>
        </div>
    @endif

@endsection

