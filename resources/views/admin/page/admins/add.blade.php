@extends('admin.layouts.app')

@section('content')
    <div class="col-md-12 center-form">
        <div class="card">
            <form method="POST" action="{{route('admin.'.$controller.'.update')}}" class="form-horizontal"
                  enctype="multipart/form-data">
                @csrf
                @if(isset($data->id))
                    <input type="hidden" name="id" value="{{$data->id}}">
                @endif
                <div class="card-header card-header-info">
                    @if($action == "add")
                        <h4 class="card-title">{{__('Ajouter un '.$status)}}</h4>
                    @else
                        <h4 class="card-title">{{__('Editer un '.$status)}}</h4>
                    @endif

                </div>
                <div class="card-body">
                    @foreach($fields as $key => $field)
                        <div class="row">
                            <label class="col-sm-2 control-lbl">
                                {{__($field)}} :
                            </label>
                            <div class="col-md-10">
                                <div class="form-group bmd-form-group is-filled">
                                    <input @if($key == "password")type="password" @else type="text" @endif
                                    class="form-control @error($key) is-invalid @enderror"
                                           name="{{$key}}"
                                           value="@if(old($key)){{old($key)}} @elseif(isset($data) && $key != "password") {{ $data->{$key} }} @endif"
                                           @if($key != "password" && $action != "edit") required @endif
                                    >

                                    @error($key)
                                    <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    @endforeach
                    @foreach($rels as $key => $relation)
                        <div class="row">
                            <label class="col-sm-2 control-lbl">
                                {{__(key($relation))}} :
                            </label>
                            <div class="col-md-10">
                                <select name="{{$key}}" class="form-control @error('{{$key}}') is-invalid @enderror">
                                    @foreach (current($relation) as $option)
                                        <option value="{{ $option->id }}"
                                                @if(old($key)==$option->id ) selected
                                                @elseif(isset($data) && $data->{$key} == $option->id) selected @endif>{{ $option->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="card-footer ml-auto text-right">
                    <a href="{{route('admin.sites.show')}}" class="btn btn-secondary"
                       data-dismiss="modal">{{ __('Cancel') }}</a>
                    <button type="submit" class="btn btn-info">
                        {{__('Ajouter')}}
                    </button>
                </div>
            </form>
        </div>
    </div>

@endsection

