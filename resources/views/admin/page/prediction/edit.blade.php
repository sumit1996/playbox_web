@extends('layouts.app', ['activePage' => 'dashboard', 'titlePage' => __('Dashboard')])

@section('content')
    @if($challenge != null)
        <div class="col-md-12 center-form">
            <div class="card">
                <form method="POST" action="{{route('prediction.update')}}" class="form-horizontal"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="card-header card-header-info">
                        <h4 class="card-title">Edit Address</h4>
                    </div>
                    <div class="card-body">
                        <input type="hidden" name="id" value="{{$challenge->id}}">


                        <div class="row">
                            <label class="col-sm-2 control-lbl">
                                Show Status :
                            </label>
                            <div class="col-md-10">
                                <div class="form-group bmd-form-group is-filled">

                                    <select id="show_status" name="show_status">
                                            <option value="">Choose Options</option>
                                            <option value="stop" @if(!empty($predictions) && $predictions->show_status == "stop") selected @endif>Stop Entry</option>
                                            <option value="result" @if(!empty($predictions) && $predictions->show_status == "result") selected @endif>Final Result</option>
                                    </select>

                                    @error('show_status')
                                    <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-sm-2 control-lbl">
                                <p class="text-uppercase"> {{$challenge->team_first}}</p>
                            </label>
                            <div class="col-md-10">
                                <div class="form-group bmd-form-group is-filled">
                                    <input type="text"
                                           class="form-control @error('answer') is-invalid @enderror"
                                           name="answer"
                                           value="@if(old('answer')){{old('answer')}} @else @if(!empty($predictions)){{$predictions->answer}} @endif @endif"
                                    >

                                    @error('answer')
                                    <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-sm-2 control-lbl">
                                <p class="text-uppercase">   {{$challenge->team_second}} </p>
                            </label>
                            <div class="col-md-10">
                                <div class="form-group bmd-form-group is-filled">
                                    <input type="text"
                                           class="form-control @error('answer1') is-invalid @enderror"
                                           name="answer1"
                                           value="@if(old('answer1')){{old('answer1')}} @else  @if(!empty($predictions)){{$predictions->answer1}} @endif @endif"
                                    >

                                    @error('answer1')
                                    <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="card-footer ml-auto text-right">
                        <a href="{{route('prediction')}}" class="btn btn-secondary"
                           data-dismiss="modal">{{ __('Cancel') }}</a>
                        <button type="submit" class="btn btn-info">
                            Update
                        </button>
                    </div>
                </form>
            </div>
        </div>
    @endif
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $(document).ready(function() {
                if ($('#challenge_type_prediction').is(':checked')) {
                    $('.team_custom').removeClass("hide");
                } else {
                    $('.team_custom').addClass("hide");
                }
                if ($('#challenge_type_video').is(':checked')) {
                    $('.video-thumbnail').removeClass("hide");
                } else {
                    $('.video-thumbnail').addClass("hide");
                }

            });
            $('#challenge_type_prediction').change(function () {
                if ($(this).is(":checked")) {
                    $('.team_custom').removeClass("hide");
                } else {
                    $('.team_custom').addClass("hide");
                }
            });
            $('#challenge_type_video').change(function(){
                if($(this).is(":checked")) {
                    $('.video-thumbnail').removeClass("hide");
                } else {
                    $('.video-thumbnail').addClass("hide");
                }
            });
        });
        $('.product-list').on('change', function () {
            $('.product-list').not(this).prop('checked', false);
        });
    </script>

@endsection

