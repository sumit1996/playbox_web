<div class="modal fade loginpop" id="deleteuser" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body center-form">
                <div class="card">

                    <div class="card-header card-header-danger">
                        <h4 class="card-title">Delete {{$title}}</h4>
                    </div>

                    <div class="card-body ">
                        <h4>Do You want to delete {{$title}} ?</h4>
                    </div>

                    <div class="card-footer ml-auto text-right">
                        <button type="button" class="btn btn-secondary"
                                data-dismiss="modal">{{ __('Cancel') }}</button>
                        <a href="#" class="btn btn-danger deletedbtn">
                            Delete
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
