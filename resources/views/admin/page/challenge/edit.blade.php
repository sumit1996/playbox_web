@extends('layouts.app', ['activePage' => 'dashboard', 'titlePage' => __('Dashboard')])

@section('content')
    @if($challenge != null)
        <div class="col-md-12 center-form">
            <div class="card">
                <form method="POST" action="{{route('challenge.update')}}" class="form-horizontal"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="card-header card-header-info">
                        <h4 class="card-title">Edit Address</h4>
                    </div>
                    <div class="card-body">
                        <input type="hidden" name="id" value="{{$challenge->id}}">


                        <div class="row">
                            <label class="col-sm-2 control-lbl">
                                Picture Challenge :
                            </label>
                            <div class="col-md-10">
                                <div class="form-group bmd-form-group is-filled">
                                    <input type="checkbox"
                                           class="form-control product-list @error('challenge_type_picture') is-invalid @enderror"
                                           name="challenge_type_picture"
                                           value="1"
                                           @if($challenge->challenge_type_picture == 1) checked @endif
                                    >
                                    <label for="vehicle1">Active</label>

                                    @error('challenge_type_picture')
                                    <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-sm-2 control-lbl">
                                Prediction Challenge :
                            </label>
                            <div class="col-md-10">
                                <div class="form-group bmd-form-group is-filled">
                                    <input type="checkbox"
                                           class="form-control product-list @error('challenge_type_prediction') is-invalid @enderror"
                                           name="challenge_type_prediction" id="challenge_type_prediction"
                                           value="1"
                                           @if($challenge->challenge_type_prediction == 1) checked @endif
                                    >
                                    <label for="vehicle1">Active</label>

                                    @error('challenge_type_prediction')
                                    <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-sm-2 control-lbl">
                                Video Challenge :
                            </label>
                            <div class="col-md-10">
                                <div class="form-group bmd-form-group is-filled">
                                    <input type="checkbox"
                                           class="form-control product-list @error('challenge_type_video') is-invalid @enderror"
                                           name="challenge_type_video"  id="challenge_type_video"
                                           value="1"
                                           @if($challenge->challenge_type_video == 1) checked @endif
                                    >
                                    <label for="vehicle1">Active</label>

                                    @error('challenge_type_video')
                                    <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row team_custom hide">
                            <label class="col-sm-2 control-lbl">
                                Team first :
                            </label>
                            <div class="col-md-10">
                                <div class="form-group bmd-form-group is-filled">
                                    <input type="text"
                                           class="form-control @error('team_first') is-invalid @enderror"
                                           name="team_first"
                                           value="@if(old('team_first')){{old('team_first')}} @else {{$challenge->team_first}} @endif"
                                           placeholder="{{ __('Team first') }}"
                                    >

                                    @error('team_first')
                                    <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row team_custom hide">
                            <label class="col-sm-2 control-lbl">
                                Team Second :
                            </label>
                            <div class="col-md-10">
                                <div class="form-group bmd-form-group is-filled">
                                    <input type="text"
                                           class="form-control @error('team_second') is-invalid @enderror"
                                           name="team_second"
                                           value="@if(old('team_second')){{old('team_second')}} @else {{$challenge->team_second}} @endif"
                                           placeholder="{{ __('Team Second') }}"
                                    >

                                    @error('team_second')
                                    <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row video-thumbnail hide">
                            <label class="col-sm-2 control-lbl">
                                Thumbnail Image :
                            </label>
                            <div class="col-md-10">
                                <div class="file-upload-input">
                                    <img
                                        src="@if($challenge->thumbnail) {{URL::asset($challenge->thumbnail)}} @else {{URL::asset('assets/img/no-preview.jpg')}} @endif"
                                        class="img-responsive img-thumbnail img-preview-tag"/>
                                    <div class="input-group">
                                        <label class="input-group-btn">
                                           <span class="btn btn-default btn-file">Browse…
                                                 <input type="file" name="thumbnail"
                                                        class=" img-preview hide @error('thumbnail') is-invalid @enderror">
                                            </span>
                                        </label>
                                        <input type="text" class="form-control" readonly disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 control-lbl">
                                Challenge Name :
                            </label>
                            <div class="col-md-10">
                                <div class="form-group bmd-form-group is-filled">
                                    <input type="text"
                                           class="form-control @error('challenge_name') is-invalid @enderror"
                                           name="challenge_name"
                                           value="@if(old('challenge_name')){{old('challenge_name')}} @else {{$challenge->challenge_name}} @endif"
                                           placeholder="{{ __('Challenge Name') }}"
                                           required
                                    >

                                    @error('challenge_name')
                                    <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-sm-2 control-lbl">
                                Game Title :
                            </label>
                            <div class="col-md-10">
                                <div class="form-group bmd-form-group is-filled">
                                    <input type="text"
                                           class="form-control @error('title') is-invalid @enderror"
                                           name="title"
                                           value="@if(old('title')){{old('title')}} @else {{$challenge->title}} @endif"
                                           required
                                    >

                                    @error('title')
                                    <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-sm-2 control-lbl">
                                Description :
                            </label>
                            <div class="col-md-10">
                                <div class="form-group bmd-form-group is-filled">
                                    <input type="text"
                                           class="form-control @error('description') is-invalid @enderror"
                                           name="description"
                                           value="@if(old('description')){{old('description')}} @else {{$challenge->description}} @endif"
                                           required
                                    >

                                    @error('description')
                                    <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-sm-2 control-lbl">
                                Hashtag :
                            </label>
                            <div class="col-md-10">
                                <div class="form-group bmd-form-group is-filled">
                                    <input type="text"
                                           class="form-control @error('hashtag') is-invalid @enderror"
                                           name="hashtag"
                                           value="@if(old('hashtag')){{old('hashtag')}} @else {{$challenge->hashtag}} @endif"
                                           required
                                    >

                                    @error('hashtag')
                                    <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-sm-2 control-lbl">
                                Image :
                            </label>
                            <div class="col-md-10">
                                <div class="file-upload-input">
                                    <img
                                        src="@if($challenge->image) {{URL::asset($challenge->image)}} @else {{URL::asset('assets/img/no-preview.jpg')}} @endif"
                                        class="img-responsive img-thumbnail img-preview-tag"/>
                                    <div class="input-group">
                                        <label class="input-group-btn">
                                           <span class="btn btn-default btn-file">Browse…
                                                 <input type="file" name="image"
                                                        class=" img-preview hide @error('image') is-invalid @enderror">
                                            </span>
                                        </label>
                                        <input type="text" class="form-control" readonly disabled>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-sm-2 control-lbl">
                                Active :
                            </label>
                            <div class="col-md-10">
                                <div class="form-group bmd-form-group is-filled">
                                    <input type="checkbox"
                                           class="form-control @error('active') is-invalid @enderror"
                                           name="active"
                                           value="1"
                                           required
                                           @if($challenge->active == 1) checked @endif
                                    >
                                    <label for="vehicle1">Active</label>

                                    @error('active')
                                    <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-sm-2 control-lbl">
                                Publish By :
                            </label>
                            <div class="col-md-10">
                                <div class="form-group bmd-form-group is-filled">
                                    {{--                                    <input type="text"--}}
                                    {{--                                           class="form-control @error('publish_by') is-invalid @enderror"--}}
                                    {{--                                           name="publish_by"--}}
                                    {{--                                           value="@if(old('publish_by')){{old('publish_by')}} @else {{$challenge->publish_by}} @endif"--}}
                                    {{--                                           required--}}
                                    {{--                                    >--}}
                                    <select id="publish_by" name="publish_by">
                                        @foreach($users as $user)
                                            <option value="{{$user->username}}"
                                                    @if($challenge->publish_by == $user->username) selected @endif
                                            ">{{$user->firstname}} {{$user->lastname}}</option>
                                        @endforeach
                                    </select>
                                    @error('publish_by')
                                    <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-sm-2 control-lbl">
                                Points :
                            </label>
                            <div class="col-md-10">
                                <div class="form-group bmd-form-group is-filled">
                                    <input type="text"
                                           class="form-control @error('points') is-invalid @enderror"
                                           name="points"
                                           value="@if(old('points')){{old('points')}} @else {{$challenge->points}} @endif"
                                           required
                                    >

                                    @error('points')
                                    <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="card-footer ml-auto text-right">
                        <a href="{{route('challenge.show')}}" class="btn btn-secondary"
                           data-dismiss="modal">{{ __('Cancel') }}</a>
                        <button type="submit" class="btn btn-info">
                            Update
                        </button>
                    </div>
                </form>
            </div>
        </div>
    @endif
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $(document).ready(function() {
                if ($('#challenge_type_prediction').is(':checked')) {
                    $('.team_custom').removeClass("hide");
                } else {
                    $('.team_custom').addClass("hide");
                }
                if ($('#challenge_type_video').is(':checked')) {
                    $('.video-thumbnail').removeClass("hide");
                } else {
                    $('.video-thumbnail').addClass("hide");
                }

            });
            $('#challenge_type_prediction').change(function () {
                if ($(this).is(":checked")) {
                    $('.team_custom').removeClass("hide");
                } else {
                    $('.team_custom').addClass("hide");
                }
            });
            $('#challenge_type_video').change(function(){
                if($(this).is(":checked")) {
                    $('.video-thumbnail').removeClass("hide");
                } else {
                    $('.video-thumbnail').addClass("hide");
                }
            });
        });
        $('.product-list').on('change', function () {
            $('.product-list').not(this).prop('checked', false);
        });
    </script>

@endsection

