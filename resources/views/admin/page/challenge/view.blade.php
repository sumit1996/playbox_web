@extends('admin.layouts.app')

@section('content')

    <div class="col-md-12 center-form">
        <div class="card">

            <div class="card-header card-header-info">
                <h4 class="card-title">{{__('Voir les adresses')}}</h4>
            </div>
            <div class="card-body">
                <div class="row">
                    <label class="col-sm-2 " style="color:black;">
                        <h4><strong>{{__('ID')}} : {{$address->id}}</strong></h4>
                    </label>
                </div>
                <div class="row">
                    <label class="col-sm-2 control-lbl">
                        {{__('Utilisateur')}} :
                    </label>
                    <div class="col-md-10">
                        <div class="form-group bmd-form-group is-filled">
                            {{ $address->user->name }}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-2 control-lbl">
                        Name :
                    </label>
                    <div class="col-md-10">
                        <div class="form-group bmd-form-group is-filled">
                            {{$address->name}}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-2 control-lbl">
                        {{__('Adresse')}} :
                    </label>
                    <div class="col-md-10">
                        <div class="form-group bmd-form-group is-filled">
                            {{$address->street.' '.$address->postcode.' , '.$address->city.' '.$address->country}}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-2 control-lbl">
                        Lat & Long :
                    </label>
                    <div class="col-md-10">
                        <div class="form-group bmd-form-group is-filled">
                            {{$address->lat.' & '.$address->lng}}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-2 control-lbl">
                        Created at :
                    </label>
                    <div class="col-md-10">
                        <div class="form-group bmd-form-group is-filled">
                            {{$address->created_at}}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-2 control-lbl">
                        Updated at :
                    </label>
                    <div class="col-md-10">
                        <div class="form-group bmd-form-group is-filled">
                            @if($address->updated_at=='') Null @else {{$address->updated_at}} @endif
                        </div>
                    </div>
                </div>

            </div>
            <div class="card-footer ml-auto text-right">
                <a href="{{route('admin.challenge.show')}}" class="btn btn-info"
                   data-dismiss="modal">{{ __('Cancel') }}</a>
            </div>
        </div>
    </div>

@endsection


