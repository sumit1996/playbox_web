@extends('layouts.app', ['activePage' => 'dashboard', 'titlePage' => __('Dashboard')])

@section('content')

    <div class="challenges-page full-width">
    <div class="col-md-12">

        <br>
        <div class="card">
            <!-- Button trigger modal -->
            <a href="{{route('challenge.add')}}" type="button" class="btn btn-info" >
                Add Challenges <i class="material-icons">add</i>
            </a>
            <div class="card-header card-header-primary">
                <h4 class="card-title ">All Challenges</h4>
                <p class="card-category">Here is all current Challenges.</p>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table user-table">
                        <thead class="text-primary">
                        <tr>
                            <th>
                                S.NO
                            </th>
                            <th>
                                Challenge Name
                            </th>
                            <th>
                                Created At
                            </th>
                            <th>
                                Updated At
                            </th>
                            <th>
                                Actions
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(!$challenges->isEmpty())
                            @foreach($challenges as $key => $challenge)
                                <tr>
                                    <td>
                                        {{$no++}}
                                    </td>
                                    <td>
                                        {{ $challenge->title }}
                                    </td>
                                    <td>
                                        {{$challenge->created_at}}
                                    </td>
                                    <td>
                                        @if($challenge->updated_at=='') Null @else {{$challenge->updated_at}} @endif
                                    </td>
                                    <td class="text-primary action-edit">
                                        <a href="{{route('challenge.edit', ['id' => $challenge->id]) }}"
                                           class="btn btn-success btn-link "><i
                                                class="material-icons">edit</i></a> <!--edit-user-btn -->
                                        |
                                        <a href="#deleteuser" data-userid="{{route('challenge.delete', ['id' => $challenge->id]) }}" data-toggle="modal" class="btn btn-danger btn-link delete-user-btn"><i class="material-icons">delete</i></a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td>
                                    Any challenge does not exist.
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


        <!-- Delete Modal -->
        @include('admin.page.delete_confirmation_popup', ['title' => 'challenge'])

    </div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>

    $('.delete-user-btn').click(function () {
        $('#deleteuser .deletedbtn').prop('href',$(this).attr('data-userid'));
    });
</script>
@endsection
