@extends('layouts.app', ['activePage' => 'challenge', 'titlePage' => __('Challenges')])


@section('content')
        <div class="col-md-12 center-form">
            <div class="card">
                <form method="POST" action="{{route('challenge.update')}}" class="form-horizontal" enctype="multipart/form-data">
                    @csrf

                    <div class="card-header card-header-info">
                        <h4 class="card-title">Add New Challenge</h4>
                    </div>
                    <div class="card-body">

                        <div class="row">
                            <label class="col-sm-2 control-lbl">
                                Picture Challenge :
                            </label>
                            <div class="col-md-10">
                                <div class="form-group bmd-form-group is-filled">
                                    <input type="checkbox"
                                           class="form-control product-list @error('challenge_type_picture') is-invalid @enderror"
                                           name="challenge_type_picture"
                                           value="1"
                                    >
                                    <label for="vehicle1">Active</label>

                                    @error('challenge_type_picture')
                                    <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 control-lbl">
                                Prediction Challenge  :
                            </label>
                            <div class="col-md-10">
                                <div class="form-group bmd-form-group is-filled">
                                    <input type="checkbox"
                                           class="form-control product-list @error('challenge_type_prediction') is-invalid @enderror"
                                           name="challenge_type_prediction" id="challenge_type_prediction"
                                           value="1"
                                    >
                                    <label for="vehicle1">Active</label>

                                    @error('challenge_type_prediction')
                                    <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 control-lbl">
                                Video Challenge  :
                            </label>
                            <div class="col-md-10">
                                <div class="form-group bmd-form-group is-filled">
                                    <input type="checkbox"
                                           class="form-control product-list @error('challenge_type_video') is-invalid @enderror"
                                           name="challenge_type_video" id="challenge_type_video"
                                           value="1"
                                    >
                                    <label for="vehicle1">Active</label>

                                    @error('challenge_type_video')
                                    <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row team_custom hide">
                            <label class="col-sm-2 control-lbl">
                                Team first :
                            </label>
                            <div class="col-md-10">
                                <div class="form-group bmd-form-group is-filled">
                                    <input type="text"
                                           class="form-control @error('team_first') is-invalid @enderror"
                                           name="team_first"
                                           value="{{old('team_first')}}"
                                           placeholder="{{ __('Team first') }}"
                                    >

                                    @error('team_first')
                                    <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row team_custom hide">
                            <label class="col-sm-2 control-lbl">
                                Team Second :
                            </label>
                            <div class="col-md-10">
                                <div class="form-group bmd-form-group is-filled">
                                    <input type="text"
                                           class="form-control @error('team_second') is-invalid @enderror"
                                           name="team_second"
                                           value="{{old('team_second')}}"
                                           placeholder="{{ __('Team Second') }}"
                                    >

                                    @error('team_second')
                                    <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row video-thumbnail hide">
                            <label class="col-sm-2 control-lbl">
                                Thumbnail Image :
                            </label>
                            <div class="col-md-10">
                                <div class="file-upload-input">
                                    <img
                                        src="{{URL::asset('assets/img/no-preview.jpg')}}"
                                        class="img-responsive img-thumbnail img-preview-tag"/>
                                    <div class="input-group">
                                        <label class="input-group-btn">
                                           <span class="btn btn-default btn-file">Browse…
                                                 <input type="file" name="thumbnail"
                                                        class=" img-preview hide @error('thumbnail') is-invalid @enderror">
                                            </span>
                                        </label>
                                        <input type="text" class="form-control" readonly disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 control-lbl">
                                Challenge Name :
                            </label>
                            <div class="col-md-10">
                                <div class="form-group bmd-form-group is-filled">
                                    <input type="text"
                                           class="form-control @error('challenge_name') is-invalid @enderror"
                                           name="challenge_name"
                                           value="{{old('challenge_name')}}"
                                           placeholder="{{ __('Challenge Name') }}"
                                           required
                                    >

                                    @error('challenge_name')
                                    <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-sm-2 control-lbl">
                             Game Title :
                            </label>
                            <div class="col-md-10">
                                <div class="form-group bmd-form-group is-filled">
                                    <input type="text"
                                           class="form-control @error('title') is-invalid @enderror"
                                           name="title"
                                           value="{{old('title')}}"
                                           placeholder="{{ __('Game Title') }}"
                                           required
                                    >

                                    @error('title')
                                    <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-sm-2 control-lbl">
                                Description :
                            </label>
                            <div class="col-md-10">
                                <div class="form-group bmd-form-group is-filled">
                                    <input type="text"
                                           class="form-control @error('description') is-invalid @enderror"
                                           name="description"
                                           value="{{old('description')}}"
                                           placeholder="{{ __('Description') }}"
                                           required
                                    >

                                    @error('description')
                                    <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-sm-2 control-lbl">
                                Hashtag :
                            </label>
                            <div class="col-md-10">
                                <div class="form-group bmd-form-group is-filled">
                                    <input type="text"
                                           class="form-control @error('hashtag') is-invalid @enderror"
                                           name="hashtag"
                                           value="{{old('hashtag')}}"
                                           placeholder="{{ __('Hashtag') }}"
                                           required
                                    >

                                    @error('hashtag')
                                    <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-sm-2 control-lbl">
                                Image :
                            </label>
                            <div class="col-md-10">
                                <div class="file-upload-input">
                                    <img
                                        src="{{URL::asset('assets/img/no-preview.jpg')}}"
                                        class="img-responsive img-thumbnail img-preview-tag"/>
                                    <div class="input-group">
                                        <label class="input-group-btn">
                                           <span class="btn btn-default btn-file">Browse…
                                                 <input type="file" name="image"
                                                        class=" img-preview hide @error('image') is-invalid @enderror">
                                            </span>
                                        </label>
                                        <input type="text" class="form-control" readonly disabled>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <label class="col-sm-2 control-lbl">
                                Active :
                            </label>
                            <div class="col-md-10">
                                <div class="form-group bmd-form-group is-filled">
                                    <input type="checkbox"
                                           class="form-control @error('active') is-invalid @enderror"
                                           name="active"
                                           value="1"
                                           required
                                    >
                                    <label for="vehicle1">Active</label>

                                    @error('active')
                                    <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-sm-2 control-lbl">
                                Publish By :
                            </label>
                            <div class="col-md-10">
                                <div class="form-group bmd-form-group is-filled">
                                    {{--                                    <input type="text"--}}
                                    {{--                                           class="form-control @error('publish_by') is-invalid @enderror"--}}
                                    {{--                                           name="publish_by"--}}
                                    {{--                                           value="@if(old('publish_by')){{old('publish_by')}} @else {{$challenge->publish_by}} @endif"--}}
                                    {{--                                           required--}}
                                    {{--                                    >--}}
                                    <select id="publish_by" name="publish_by">
                                        @foreach($users as $user)
                                            <option value="{{$user->username}}">{{$user->firstname}} {{$user->lastname}}</option>
                                        @endforeach
                                    </select>
                                    @error('publish_by')
                                    <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-sm-2 control-lbl">
                                Points :
                            </label>
                            <div class="col-md-10">
                                <div class="form-group bmd-form-group is-filled">
                                    <input type="text"
                                           class="form-control @error('points') is-invalid @enderror"
                                           name="points"
                                           value="{{old('points')}}"
                                           placeholder="{{ __('Points') }}"
                                           required
                                    >

                                    @error('points')
                                    <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="card-footer ml-auto text-right">
                        <a href="{{route('challenge.show')}}" class="btn btn-secondary"
                                data-dismiss="modal">{{ __('Cancel') }}</a>
                        <button type="submit" class="btn btn-info">
                            Add
                        </button>
                    </div>
                </form>
            </div>
        </div>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script type="text/javascript">
            $('#challenge_type_prediction').change(function(){
                if($(this).is(":checked")) {
                    $('.team_custom').removeClass("hide");
                } else {
                    $('.team_custom').addClass("hide");
                }
            });
            $('#challenge_type_video').change(function(){
                if($(this).is(":checked")) {
                    $('.video-thumbnail').removeClass("hide");
                } else {
                    $('.video-thumbnail').addClass("hide");
                }
            });
            $('.product-list').on('change', function() {
                $('.product-list').not(this).prop('checked', false);
            });
        </script>
@endsection

