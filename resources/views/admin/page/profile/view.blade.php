@extends('admin.layouts.app')

@section('content')

    <div class="col-md-12 center-form">
        <div class="card">
            <div class="card-header card-header-info">
                <h4 class="card-title">Profile</h4>
            </div>
            @if($adminUser!='')
                <div class="card-body">
                    <div class="row">
                        <label class="col-sm-2 control-lbl">
                            Name :
                        </label>
                        <div class="col-md-10">
                            <div class="form-group bmd-form-group is-filled">
                                {{$adminUser->name}}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-2 control-lbl">
                            Email :
                        </label>
                        <div class="col-md-10">
                            <div class="form-group bmd-form-group is-filled">
                                 {{$adminUser->email}}
                            </div>
                        </div>
                    </div>



                    <div class="row">
                        <label class="col-sm-2 control-lbl">
                            Created at :
                        </label>
                        <div class="col-md-10">
                            <div class="form-group bmd-form-group is-filled">
                                 {{$adminUser->created_at}}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-2 control-lbl">
                            Updated at :
                        </label>
                        <div class="col-md-10">
                            <div class="form-group bmd-form-group is-filled">
                                @if($adminUser->updated_at=='') Null @else {{$adminUser->updated_at}} @endif
                            </div>
                        </div>
                    </div>

                </div>
                <div class="card-footer ml-auto text-right">
                    <a href="{{route('admin.dashboard')}}" class="btn btn-secondary"
                       data-dismiss="modal">{{ __('Cancel') }}</a>
                    <a href="{{route('admin.profile.edit')}}" class="btn btn-info">
                        Edit
                    </a>
                </div>
            @endif


        </div>
    </div>

@endsection
