@extends('admin.layouts.app')

@section('content')

    <div class="col-md-12 center-form">
        <div class="card">
            <form method="POST" action="{{route('admin.profile.update')}}" class="form-horizontal">
                @csrf
                <div class="card-header card-header-info">
                    <h4 class="card-title">Edit Profile</h4>
                </div>
                <div class="card-body">
                    <input type="hidden" name="id" value="{{$adminUser->id}}">
                    <div class="row">
                        <label class="col-sm-2 control-lbl">
                            Name :
                        </label>
                        <div class="col-md-10">
                            <div class="form-group bmd-form-group is-filled">
                                <input type="text"
                                       class="form-control @error('name') is-invalid @enderror"
                                       name="name"
                                       value="@if(old('name')){{old('name')}} @else {{$adminUser->name}} @endif"
                                       placeholder="{{ __('Name') }}"
                                       required>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-2 control-lbl">
                            Email :
                        </label>
                        <div class="col-md-10">
                            <div class="form-group bmd-form-group is-filled">
                                <input type="text"
                                       class="form-control @error('email') is-invalid @enderror"
                                       name="email"
                                       value="@if(old('email')){{old('email')}} @else {{$adminUser->email}} @endif"
                                       placeholder="{{ __('Email') }}"
                                       required>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                </div>

                <div class="card-footer ml-auto text-right">
                    <a href="{{route('admin.profile')}}" class="btn btn-secondary"
                       data-dismiss="modal">{{ __('Cancel') }}</a>
                    <button type="submit" class="btn btn-info">
                        Update
                    </button>
                </div>
            </form>


        </div>
    </div>

    <div class="col-md-12 center-form">
        <div class="card">
            <form method="POST" action="{{route('admin.profile.pswd')}}" class="form-horizontal">
                @csrf
                <div class="card-header card-header-info">
                    <h4 class="card-title">Change Password</h4>
                </div>
                <div class="card-body">
                    <input type="hidden" name="id" value="{{$adminUser->id}}">
                    <div class="row">
                        <label class="col-sm-2 control-lbl">
                            Current Password :
                        </label>
                        <div class="col-md-10">
                            <div class="form-group bmd-form-group is-filled">
                                <input type="password"
                                       class="form-control @error('current_password') is-invalid @enderror"
                                       name="current_password"
                                       placeholder="{{ __('Current Password') }}"
                                       required
                                >

                                @error('current_password')
                                <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-2 control-lbl">
                            New Password :
                        </label>
                        <div class="col-md-10">
                            <div class="form-group bmd-form-group is-filled">
                                <input type="password"
                                       class="form-control @error('new_password') is-invalid @enderror"
                                       name="new_password"
                                       placeholder="{{ __('New Password') }}"
                                       required
                                >

                                @error('new_password')
                                <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-2 control-lbl">
                            New Confirm Password :
                        </label>
                        <div class="col-md-10">
                            <div class="form-group bmd-form-group is-filled">
                                <input type="password"
                                       class="form-control @error('new_confirm_password') is-invalid @enderror"
                                       name="new_confirm_password"
                                       placeholder="{{ __('Confirm Password') }}"
                                       required
                                >

                                @error('new_confirm_password')
                                <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                </div>

                <div class="card-footer ml-auto text-right">
                    <a href="{{route('admin.profile')}}" class="btn btn-secondary"
                       data-dismiss="modal">{{ __('Cancel') }}</a>
                    <button type="submit" class="btn btn-info">
                        Update
                    </button>
                </div>
            </form>


        </div>
    </div>

@endsection
