<!DOCTYPE html>
<html>
<head>
    <title></title>
    <!-- meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.css"/>
    <script src="{{ asset('js/calendar/jquery-ui.min.js')}}"></script>
    <script src="{{ asset('js/calendar/moment.min.js')}}"></script>
    <script src="{{ asset('js/calendar/fullcalendar.min.js')}}"></script>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script>
        $(document).ready(function () {

            $('#save').click(function () {
                var eventsFromCalendar = $('#calendar').fullCalendar('clientEvents');
                console.log(eventsFromCalendar);
                var events_global = new Array();
                for(i=0, size=eventsFromCalendar.length; i<size; i++){
                    range = "non";
                    if(eventsFromCalendar[i]['end']){
                        range = "oui";
                    }
                    var eventdata = {title:eventsFromCalendar[i]['title'], start:eventsFromCalendar[i]['start'], end:eventsFromCalendar[i]['end'], range:range };
                    events_global[i] = eventdata;

                }
                console.log(events_global);
                jsons = JSON.stringify(events_global);
                console.log(jsons);
                $.ajax({
                    url: '{{route('admin.calendar_save',['id'=>$id])}}',
                    type: "POST",
                    data: {eventsJson: jsons},
                    dataType: "json",
                    success: function (json) {
                        if(json.success)
                            alert('save'); // save popup
                        else
                            alert('try again'); // try again popup
                    }
                });
            });

            var domain = window.location.origin;
            var priceId = $('#field').data("field-id");
            var title = "Special of the Day";
            var calendar = $('#calendar').fullCalendar({
                editable: true,
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month',//,agendaWeek,agendaDay'
                },
                events: domain + '/admin/load/' + priceId,
                selectable: true,
                selectHelper: true,
                selectOverlap: function(event) {
                    return event.rendering === 'background';
                },
                select: function(start, end) {

                    // get today's date
                    var today = new Date();
                    var dd = today.getDate();

                    var mm = today.getMonth()+1;
                    var yyyy = today.getFullYear();
                    if(dd<10)
                    {
                        dd='0'+dd;
                    }

                    if(mm<10)
                    {
                        mm='0'+mm;
                    }
                    today = yyyy+'-'+mm+'-'+dd;
                    //console.log(today);

                    if(start.isBefore(moment(today))) {
                        $('#calendar').fullCalendar('unselect');
                        return false;
                    }

                    // var title = prompt('Price:');
                    var eventData;
                        eventData = {
                            title: title,
                            start: start,
                            end: end
                        };


                        $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true

                    $('#calendar').fullCalendar('unselect');

                },
               editable: true,
                eventLimit: true, // allow "more" link when too many events
                navLinks: true,
                eventRender: function (event, element) {
                    element.append("<span class='closeon'>X</span>");
                    element.find(".closeon").click(function () {
                        $('#calendar').fullCalendar('removeEvents', event._id);
                    });
                }
            });

            $('button.save-all-events.cancel').on('click', function () {
                $(window.parent.document).find('.fancybox-container').fadeOut('swing');
                $(window.parent.document).find('body').removeClass('compensate-for-scrollbar');

                setTimeout(function () {
                    $(window.parent.document).find('.fancybox-container').remove();
                    $(window.parent.document).find('body').removeClass('compensate-for-scrollbar');
                },600);
            });
        });

    </script>

    <style>
        /*------ calendar-block ------*/
        .calendar-block {
            padding: 40px 0;
          }

        /*------ save-all-events ------*/
        .fc button.save-all-events,
        .save-all-events {
            border: none;
            background: #000;
            color: #fff;
            font-size: 14px;
            width: 112px;
            height: 30px;
            margin-left: 11px;
            outline: none;
        }

        .calendar-block td.fc-event-container {
            text-align: center;
        }

        .calendar-block span.fc-title:before {
            font-size: 80%;
            position: relative;
            top: -2px;
            margin-right: 2px;
        }

        /*------ closeon ------*/
        .calendar-block span.closeon {
            color: #fff;
            font-weight: 500;
            position: absolute;
            left: 5px;
            top: 0;
            font-size: 13px;
            z-index:100;
        }
        .fancybox-slide {
            padding: 44px 0;
        }

        /*------ save-all-events:before ------*/
        .fc button.save-all-events:before,
        .save-all-events:before {
            content: "";
            position: absolute;
            z-index: 1;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            background: rgba(255, 255, 255, 0.22);
            transform: scaleX(0);
            -webkit-transform: scaleX(0);
            -ms-transform: scaleX(0);
            -moz-transform: scaleX(0);
            -o-transform: scaleX(0);
            transform-origin: 50%;
            transition-property: transform;
            transition: 0.3s;
            -webkit-transition: 0.3s;
            -ms-transition: 0.3s;
            -moz-transition: 0.3s;
            -o-transition: 0.3s;
            transition-timing-function: ease-out;
        }

        .fc button.save-all-events:hover:before,
        .save-all-events:hover:before {
            transform: scaleX(1);
            -webkit-transform: scaleX(1);
            -ms-transform: scaleX(1);
            -moz-transform: scaleX(1);
            -o-transform: scaleX(1);
        }

        /*------ save-all-events > span ------*/
        .fc button.save-all-events span,
        .save-all-events span {
            position: relative;
            z-index: 2;
        }

        .fc button.save-all-events:first-child,
        .save-all-events:first-child {
            margin-left: 0px;
        }

        /*------ save-all-events cancel ------*/
        .fc button.save-all-events.cancel,
        .save-all-events.cancel {
            background: #858585;
        }

        .fc-toolbar .fc-center h2 {
            font-size: 28px;
        }

        /*------ today,fc-event ------*/
        .calendar-block .fc-event,
        .calendar-block .fc-highlight,
        .calendar-block .fc-event-dot {
            background-color: #0bd744;
            border: 0;
        }
        .calendar-block .fc-unthemed td.fc-today{
            background-color: inherit;
        }

        /*------ fc-event,fc-event hover ------*/
        .calendar-block table .fc-event,
        .calendar-block table .fc-event:hover {
            color: #ffffff;
            border-radius: 0;
        }

        .full-calender.container {
            width: 90% !important;
        }

        .fc-event-container .fc-day-grid-event .fc-time {
            display: none;
        }
        button.fancybox-button.fancybox-button--close {
            width: 40px;
            height: 40px;
            margin-top: 59px;
            left: -4px;
            position: relative;
            opacity: 1;
            z-index: 1000;
        }
        span.fc-title {
            padding: 2px;
            display: inline-block;
            line-height: 18px;
        }
        button.save-all-events.cancel.popup {
            width: 35px;
            height: 35px;
            padding-top: 6px;
            position: absolute;
            top: 0;
            right: 0;
            color: #fff;
            cursor: pointer;
            z-index: 1000000000;
            margin-right: 0;
        }
        button.save-all-events.cancel.popup svg {
            color: #ffffff;
            fill: #ffffff;
        }

    </style>
</head>
<body>
<div id="field" data-field-id="{{$id}}"></div>

<!--======= calendar-block =======-->
<div class="container">
    <div class="calendar-block">
        <div id="calendar"></div>
    </div>
</div>
<!--==== close calendar-block ====-->

{{--======== cancel-save ========--}}
<!--<a href="{{ route('admin.items.show') }}" class="save-all-events cancel popup"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 10.6L6.6 5.2 5.2 6.6l5.4 5.4-5.4 5.4 1.4 1.4 5.4-5.4 5.4 5.4 1.4-1.4-5.4-5.4 5.4-5.4-1.4-1.4-5.4 5.4z"></path></svg></a>-->
<div class="cancel-save">
    <a href="{{ route('admin.items.show') }}" class="save-all-events cancel"><span>BACK</span></a>
    <button id="save" class="save-all-events"><span>SAVE</span></button>
</div>
{{--===== close cancel-save =====--}}


{{--======= document ready =======--}}
<script>
    $(document).ready(function () {
        setInterval(function () {
            $('#calendar .fc-month-button').replaceWith($('.cancel-save'));
            $('.calendar-block').parent('.container').addClass('full-calender');
        }, 500);
    });
</script>


</body>
</html>

