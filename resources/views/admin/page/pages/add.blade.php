@extends('admin.layouts.app')

@section('content')
    <div class="col-md-12 center-form">
        <div class="card">
            <form method="POST" action="{{route('admin.'.$controller.'.update')}}" class="form-horizontal"
                  enctype="multipart/form-data">
                @csrf
                @if(isset($data->id))
                    <input type="hidden" name="id" value="{{$data->id}}">
                @endif
                <div class="card-header card-header-info">
                    @if($action == "add")
                        <h4 class="card-title">{{__('Ajouter un '.$status)}}</h4>
                    @else
                        <h4 class="card-title">{{__('Editer un '.$status)}}</h4>
                    @endif

                </div>
                <div class="card-body">
                    @foreach($fields as $key => $field)
                        <div class="row">
                            <label class="col-sm-2 control-lbl">
                                {{__($field)}} :
                            </label>

                            @if($key == "image")

                                <div class="col-md-10">
                                    <div class="file-upload-input ">
                                        @if($action != "edit")

                                            <img src="{{URL::asset('assets/img/no-preview.jpg')}}"
                                                 class="img-responsive img-thumbnail img-preview-tag"/>
                                        @else

                                            <img
                                                    src="@if($data->image) {{URL::asset($data->image)}} @else {{URL::asset('assets/img/no-preview.jpg')}} @endif"
                                                    class="img-responsive img-thumbnail img-preview-tag"/>
                                        @endif
                                        <div class="resp-container-wrap">
                                            <div class="resp-container">
                                                <iframe class="resp-iframe preview-file" src="" gesture="media"
                                                        allow="encrypted-media" allowfullscreen></iframe>
                                            </div>
                                        </div>

                                        <div class="input-group">
                                            <label class="input-group-btn">
                                            <span class="btn btn-default btn-file">Browse…
                                                 <input type="file" name="images"
                                                        class=" img-preview hide @error('images') is-invalid @enderror">
                                            </span>
                                            </label>
                                            <input type="text" class="form-control" readonly disabled>
                                        </div>
                                    </div>
                                </div>



                            @elseif($key == "active")


                                <div class="col-md-10">
                                    <div class="form-group bmd-form-group is-filled">
                                        <input type="checkbox"
                                               class="form-control @error('name') is-invalid @enderror"
                                               name="active"
                                               value="1" @if(isset($data) && $data->active == "1") checked @endif
                                        >

                                    </div>
                                </div>


                            @elseif($key == "body")

                                <textarea
                                        id="summary-ckeditor"
                                        class="form-control @error('{{$key}}') is-invalid @enderror"
                                        name="{{$key}}"
                                        value=""
                                        required>@if(old($key)){{old($key)}} @elseif(isset($data)) {{ $data->{$key} }} @endif</textarea>
                                @error('{{$key}}')
                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                @enderror

                            @else

                                <div class="col-md-10">
                                    <div class="form-group bmd  -form-group is-filled">
                                        <input type="text"
                                               class="form-control @error($key) is-invalid @enderror"
                                               name="{{$key}}"
                                               value="@if(old($key)){{old($key)}} @elseif(isset($data)) {{ $data->{$key} }} @endif"
                                        >

                                        @error($key)
                                        <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                            @endif

                        </div>
                    @endforeach
                    @foreach($rels as $key => $relation)
                        <div class="row">
                            <label class="col-sm-2 control-lbl">
                                {{__(key($relation))}} :
                            </label>
                            <div class="col-md-10">
                                <select name="{{$key}}" class="form-control @error('{{$key}}') is-invalid @enderror">
                                    @foreach (current($relation) as $option)
                                        <option value="{{ $option->id }}"
                                                @if(old($key)==$option->id ) selected
                                                @elseif(isset($data) && $data->{$key} == $option->id) selected @endif>{{ $option->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="card-footer ml-auto text-right">
                    <a href="{{route('admin.sites.show')}}" class="btn btn-secondary"
                       data-dismiss="modal">{{ __('Cancel') }}</a>
                    <button type="submit" class="btn btn-info">
                        {{__('Ajouter')}}
                    </button>
                </div>
            </form>
        </div>
    </div>

    <!-- ck editor -->
    <script src="//cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('summary-ckeditor', {
            filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
    </script>
    <!-- ck editor -->
@endsection

