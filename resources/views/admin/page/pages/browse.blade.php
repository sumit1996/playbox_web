@extends('admin.layouts.app')

@section('content')

    <div class="addresses-page full-width">
        <div class="col-md-12">
            <!-- Button trigger modal -->
            <a href="{{route('admin.pages.add')}}" type="button" class="btn btn-info">
                {{__("Ajouter un ".$status)}} <i class="material-icons">add</i>
            </a>
            <br>
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title ">{{__('Liste des')}} {{__($item)}}</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table user-table">
                            <thead class=" text-primary">
                            <tr>
                                @foreach($cols as $key => $column)
                                    <th>
                                        {{__($column)}}
                                    </th>
                                @endforeach
                            </tr>
                            </thead>
                            <tbody>
                            @if(!$data->isEmpty())
                                @foreach($data as $key => $row)
                                    <tr>

                                        @foreach($cols as $key => $column)
                                            <td>
                                                {{ $row->{$key} }}
                                            </td>
                                        @endforeach
                                        <td class="text-primary action-edit">
                                            <a href="{{route('admin.pages.edit', ['id' => $row->id]) }}"
                                               class="btn btn-success btn-link "><i
                                                        class="material-icons">edit</i></a> <!--edit-user-btn -->
                                            |
                                            <a href="#deleteuser"
                                               data-userid="{{route('admin.pages.delete', ['id' => $row->id]) }}"
                                               data-toggle="modal" class="btn btn-danger btn-link delete-user-btn"><i
                                                        class="material-icons">delete</i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td>
                                        {{__('Cette liste est vide pour le moment.')}}
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- Delete Modal -->
        @include('admin.page.delete_confirmation_popup', ['title' => 'site'])
    </div>

@endsection
