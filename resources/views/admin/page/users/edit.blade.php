@extends('admin.layouts.app')

@section('content')
    @if($user!=null)
        <div class="col-md-12 center-form">
            <div class="card">
                <form method="POST" action="{{route('admin.users.update')}}" class="form-horizontal">
                    @csrf
                    <div class="card-header card-header-info">
                        <h4 class="card-title">Edit User</h4>
                    </div>
                    <div class="card-body">
                        <input type="hidden" name="id" value="{{$user->id}}">
                        <div class="row">
                            <label class="col-sm-2 control-lbl">
                                Name :
                            </label>
                            <div class="col-md-10">
                                <div class="form-group bmd-form-group is-filled">
                                    <input type="text"
                                           class="form-control @error('name') is-invalid @enderror"
                                           name="name"
                                           value="@if(old('name')) {{old('name')}} @else {{ $user->name}} @endif"
                                           required
                                    >

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <label class="col-md-2 control-lbl">
                                Email :
                            </label>
                            <div class="col-md-10">
                                <div class="form-group bmd-form-group is-filled">
                                    <input type="text"
                                           class="form-control @error('email') is-invalid @enderror"
                                           name="email"
                                           value="@if(old('email')) {{old('email')}} @else {{ $user->email}} @endif"
                                           placeholder="{{ __('Email') }}" required
                                           autocomplete="email"
                                    >
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                            </div>
                        </div>

                        {{--<div class="row">
                            <label class="col-sm-2 control-lbl">
                                Role :
                            </label>
                            <div class="col-md-10">
                                <div class="form-group bmd-form-group is-filled">
                                    <select name="role" class="form-control @error('role') is-invalid @enderror">
                                        @foreach (DB::table('roles')->where('id','!=',1)->get() as $role)
                                            <option value="{{ $role->id }}"
                                                    @if($user->role_id==$role->id) selected @endif>{{ $role->display_name }}</option>
                                        @endforeach
                                    </select>
                                    @error('role')
                                    <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>--}}

                        <div class="row">
                            <label class="col-sm-2 control-lbl">
                                Site :
                            </label>
                            <div class="col-md-10">
                                <div class="form-group bmd-form-group is-filled">
                                    <select name="site" id="site"
                                            class="change-user form-control @error('site') is-invalid @enderror">
                                        @foreach (\App\Site::all() as $site)
                                            <option value="{{ $site->id }}"
                                                    @if($user->site_id==$site->id ) selected @endif>{{ $site->title }}</option>
                                        @endforeach
                                    </select>
                                    @error('site')
                                    <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="card-footer ml-auto text-right">
                        <a href="{{route('admin.users.show')}}" class="btn btn-secondary"
                           data-dismiss="modal">{{ __('Cancel') }}</a>
                        <button type="submit" class="btn btn-info">
                            Update
                        </button>
                    </div>
                </form>
            </div>
        </div>
    @endif

@endsection

