@extends('admin.layouts.app')

@section('content')
    <div class="users-page full-width user-view-page">
    <div class="col-md-12 center-form">
        <div class="card">

            <div class="card-header card-header-info">
                <h4 class="card-title">View User</h4>
            </div>
            <div class="card-body">
                <div class="row">
                    <label class="col-sm-2 " style="color:black;">
                        <h4><strong>ID : {{$user->id}}</strong></h4>
                    </label>
                </div>

                <div class="row">
                    <label class="col-sm-2 control-lbl">
                        Name :
                    </label>
                    <div class="col-md-10">
                        <div class="form-group bmd-form-group is-filled">
                            {{$user->name}}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-2 control-lbl">
                        Email :
                    </label>
                    <div class="col-md-10">
                        <div class="form-group bmd-form-group is-filled">
                            {{$user->email}}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-2 control-lbl">
                        Role :
                    </label>
                    <div class="col-md-10">
                        <div class="form-group bmd-form-group is-filled">
                            {{ \App\Helpers\Helper::fetch_user_is_buyer_seller_or_both($user->id) }}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-2 control-lbl">
                        Site :
                    </label>
                    <div class="col-md-10">
                        <div class="form-group bmd-form-group is-filled">
                            @if($siteTitle = \App\Site::getSiteById($user->site_id))
                                {{$siteTitle['title']}}
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-2 control-lbl">
                        Created at :
                    </label>
                    <div class="col-md-10">
                        <div class="form-group bmd-form-group is-filled">
                            {{$user->created_at}}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-2 control-lbl">
                        Updated at :
                    </label>
                    <div class="col-md-10">
                        <div class="form-group bmd-form-group is-filled">
                            @if($user->updated_at=='') Null @else {{$user->updated_at}} @endif
                        </div>
                    </div>
                </div>

            </div>
            <div class="card-footer ml-auto text-right">
                <a href="{{route('admin.users.show')}}" class="btn btn-info"
                   data-dismiss="modal">{{ __('Cancel') }}</a>
            </div>
        </div>
    </div>
    </div>

@endsection


