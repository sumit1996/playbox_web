@extends('admin.layouts.app')

@section('content')

    <div class="users-page full-width">
        <div class="col-md-12">
            <!-- Button trigger modal -->
            <a href="{{route('admin.users.add')}}" type="button" class="btn btn-info">
                Add User <i class="material-icons">add</i>
            </a>
            <br>
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title ">All Users</h4>
                    <p class="card-category"> Here is all current users.</p>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table user-table">
                            <thead class=" text-primary">
                            <tr>
                                <th>
                                    S.NO
                                </th>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Email
                                </th>
                                {{--<th>
                                    Role
                                </th>--}}
                                <th>
                                    Site
                                </th>
                                <th>
                                    Created At
                                </th>
                                {{--<th>
                                    Updated At
                                </th>--}}
                                <th>
                                    Actions
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!$users->isEmpty())
                                @foreach($users as $key => $user)
                                    <tr>
                                        <td>
                                            {{$no++}}
                                        </td>
                                        <td>
                                            {{$user->name}}
                                        </td>
                                        <td>
                                            {{$user->email}}
                                        </td>
                                        {{--<td>
                                            {{ \App\Helpers\Helper::fetch_user_is_buyer_seller_or_both($user->id) }}
                                        </td>--}}
                                        <td>
                                            @if($siteTitle = \App\Site::getSiteById($user->site_id))
                                                {{$siteTitle['title']}}
                                            @endif
                                        </td>
                                        <td>
                                            {{$user->created_at}}
                                        </td>
                                        {{--<td>
                                            @if($user->updated_at=='') Null @else {{$user->updated_at}} @endif
                                        </td>--}}
                                        <td class="text-primary action-edit">
                                            <a href="{{route('admin.users.edit', ['id' => $user->id]) }}"
                                               class="btn btn-success btn-link "><i
                                                    class="material-icons">edit</i></a> <!--edit-user-btn -->
                                            |
                                            <a href="{{route('admin.users.view', ['id' => $user->id]) }}"
                                               class="btn btn-info btn-link view-user-btn">
                                                <i class="material-icons">remove_red_eye</i>
                                            </a>
                                            |
                                            <a href="#deleteuser"
                                               data-userid="{{route('admin.users.delete', ['id' => $user->id]) }}"
                                               data-toggle="modal" class="btn btn-danger btn-link delete-user-btn"><i
                                                    class="material-icons">delete</i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td>
                                        Any user not exist.
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- Delete Modal -->
        @include('admin.page.delete_confirmation_popup', ['title' => 'user'])

    </div>
@endsection
