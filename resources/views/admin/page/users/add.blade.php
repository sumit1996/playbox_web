@extends('admin.layouts.app')

@section('content')
    <div class="col-md-12 center-form">
        <div class="card">
            <form method="POST" action="{{route('admin.users.update')}}" class="form-horizontal">
                @csrf

                <div class="card-header card-header-info">
                    <h4 class="card-title">Add New User</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <label class="col-sm-2 control-lbl">
                            Name :
                        </label>
                        <div class="col-md-10">
                            <div class="form-group bmd-form-group is-filled">
                                <input type="text"
                                       class="form-control @error('name') is-invalid @enderror"
                                       name="name"
                                       value="{{old('name')}}"
                                       placeholder="{{ __('Name') }}"
                                       required
                                >

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <label class="col-md-2 control-lbl">
                            Email :
                        </label>
                        <div class="col-md-10">
                            <div class="form-group bmd-form-group is-filled">
                                <input type="text"
                                       class="form-control @error('email') is-invalid @enderror"
                                       name="email" value="{{old('email')}}"
                                       placeholder="{{ __('Email') }}" required
                                       autocomplete="email"
                                >
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-2 control-lbl">
                            Password :
                        </label>
                        <div class="col-md-10">
                            <div class="form-group bmd-form-group is-filled">
                                <input type="password"
                                       class="form-control @error('password') is-invalid @enderror"
                                       name="password"
                                       placeholder="{{ __('Password') }}"
                                       required
                                >

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-2 control-lbl">
                            Confirm Password :
                        </label>
                        <div class="col-md-10">
                            <div class="form-group bmd-form-group is-filled">
                                <input type="password"
                                       class="form-control @error('confirm_password') is-invalid @enderror"
                                       name="confirm_password"
                                       placeholder="{{ __('Confirm Password') }}"
                                       required
                                >

                                @error('confirm_password')
                                <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    {{--<div class="row">
                        <label class="col-sm-2 control-lbl">
                            Role :
                        </label>
                        <div class="col-md-10">
                            <div class="form-group bmd-form-group is-filled">
                                <select name="role" class="form-control @error('role') is-invalid @enderror">
                                    <option value="">--- Select Role ---</option>
                                    @foreach (DB::table('roles')->where('id','!=',1)->get() as $role)
                                        <option value="{{ $role->id }}"
                                                @if(old('role')==$role->id) selected @endif>{{ $role->display_name }}</option>
                                    @endforeach
                                </select>

                                @error('role')
                                <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>--}}

                    <div class="row">
                        <label class="col-sm-2 control-lbl">
                            Site :
                        </label>
                        <div class="col-md-10">
                            <div class="form-group bmd-form-group is-filled">
                                <select name="site" id="site"
                                        class="change-user form-control @error('site') is-invalid @enderror">
                                    <option value="">--- Select Site ---</option>
                                    @foreach (\App\Site::all() as $site)
                                        <option value="{{ $site->id }}"
                                                @if(old('site')==$site->title ) selected @endif>{{ $site->title }}</option>
                                    @endforeach
                                </select>
                                @error('site')
                                <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                </div>

                <div class="card-footer ml-auto text-right">
                    <a href="{{route('admin.users.show')}}" class="btn btn-secondary"
                       data-dismiss="modal">{{ __('Cancel') }}</a>
                    <button type="submit" class="btn btn-info">
                        Add
                    </button>
                </div>
            </form>
        </div>
    </div>


@endsection

