@if(isset($middle_office) && $middle_office)
    <div class="form-group bmd-form-group is-filled">
        <select name="sub_category_id" id="sub_category_id"
                class="change-user form-control select-bg" required>
            <option value="">-- Sélectionnez une sous catégorie --</option>
            @foreach($result as $res)
                <option value="{{ $res->id }}"
                        @if(isset($category_id) && $category_id==$res->id ) selected @endif>{{ ucfirst($res->name) }}</option>
            @endforeach
        </select>

    </div>

@else

    <label class="col-sm-2 control-lbl">
        Sub Category :
    </label>
    <div class="col-md-10">
        <div class="form-group bmd-form-group is-filled">
            <select name="sub_category_id" id="sub_category_id"
                    class="change-user form-control" required>
                <option value="">-----------</option>
                @foreach($result as $res)
                    <option value="{{ $res->id }}"
                            @if(isset($category_id) && $category_id==$res->id ) selected @endif>{{ ucfirst($res->name) }}</option>
                @endforeach
            </select>

        </div>
    </div>

@endif

