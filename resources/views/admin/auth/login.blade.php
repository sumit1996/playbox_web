@extends('admin.auth.layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">

                <form method="POST" action="{{ route('admin.post.login') }}" class="form-horizontal">
                    @csrf
                    <div class="card">

                        <div class="card-header card-header-primary">
                            <h4 class="card-title">{{ __('Login') }}</h4>
                        </div>

                        <div class="card-body ">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group bmd-form-group is-filled">
                                        <input id="email" type="email"
                                               class="form-control @error('email') is-invalid @enderror"
                                               name="email" value="{{ old('email') }}"
                                               placeholder="{{ __('E-Mail Address') }}" required autocomplete="email"
                                               autofocus>
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group bmd-form-group is-filled">
                                        <input id="password" type="password"
                                               class="form-control @error('password') is-invalid @enderror"
                                               name="password"
                                               required autocomplete="current-password"
                                               placeholder="{{ __('Password') }}">

                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="checkbox" value="" checked=""
                                                   name="remember"
                                                   id="remember" {{ old('remember') ? 'checked' : '' }}>
                                            <span class="form-check-sign">
                                          <span class="check"></span>
                                        </span>

                                            {{ __('Souviens-toi de moi') }} {{--Rester connecté--}}
                                        </label>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="card-footer ml-auto mr-auto">
                            <button type="submit" class="btn btn-primary">{{ __('Login') }}</button>

                            {{--@if (Route::has('password.request'))
                                       <a class="btn btn-link" href="{{ route('password.request') }}">
                                           {{ __('Forgot Your Password?') }}
                                       </a>
                                   @endif--}}
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
