<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport'/>
    <title>{{ config('app.name', 'ITOU') }}</title>
    <link rel="apple-touch-icon" sizes="76x76" href="{{URL::asset('assets/img/apple-icon.png')}}">

    <!--   Core JS Files   -->
    <script src="{{URL::asset('assets')}}/js/core/jquery.min.js"></script>

    <!-- Scripts -->
    {{--<script src="{{URL::asset('assets/js/app.js')}}" defer></script>--}}
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <link rel="icon" type="image/png" href="{{URL::asset('assets')}}/img/favicon.png">

    <!-- Fonts and icons -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">

    <!-- CSS Files -->
    <link href="{{URL::asset('vendor/css/material-dashboard.min.css')}}" rel="stylesheet" />
    {{--<link href="https://demos.creative-tim.com/material-dashboard//assets/css/material-dashboard.min.css?v=2.1.1" rel="stylesheet" />--}}
    <link href="{{URL::asset('assets/dist/style.css')}}" rel="stylesheet"/>

    <!-- Editor -->
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css">

</head>

<body class="">
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NKDMSK6"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="wrapper ">

    @include('admin.layouts.sidebar')

    <div class="main-panel ps-container ps-theme-default">

        <div class="content">
            <div class="container-fluid">
                @include('admin.layouts.navbar')

                <div class="row">
                    @yield('content')
                </div>
            </div>
        </div>


        {{--<footer class="footer">
            <div class="container-fluid">
                <nav class="float-left">
                    <ul>
                        <li></li>
                    </ul>
                </nav>
                <div class="copyright float-right">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>
                    , made with <i class="material-icons">favorite</i>
                </div>
            </div>
        </footer>--}}
    </div>
</div>
{{--
<div class="fixed-plugin">
    <div class="dropdown show-dropdown">
        <a href="#" data-toggle="dropdown">
            <i class="fa fa-cog fa-2x"> </i>
        </a>
        <ul class="dropdown-menu">
            <li class="header-title">Sidebar Filters</li>
            <li class="adjustments-line">
                <a href="javascript:void(0)" class="switch-trigger active-color">
                    <div class="badge-colors ml-auto mr-auto">
                        <span class="badge filter badge-purple" data-color="purple"></span>
                        <span class="badge filter badge-azure" data-color="azure"></span>
                        <span class="badge filter badge-green" data-color="green"></span>
                        <span class="badge filter badge-warning active" data-color="orange"></span>
                        <span class="badge filter badge-danger" data-color="danger"></span>
                        <span class="badge filter badge-rose" data-color="rose"></span>
                    </div>
                    <div class="clearfix"></div>
                </a>
            </li>
        </ul>
    </div>
</div>--}}



<!--====== JS Files ======-->

<!-- Bootstrap tags input -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js"></script>
<!-- Type ahead -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/1.3.1/typeahead.bundle.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/1.3.1/bloodhound.min.js" type="text/javascript"></script>
<!-- /Bootstrap tags input -->

<!-- Editor js -->
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<!-- custom-admin -->
<script src="{{URL::asset('assets/js/custom-admin.js')}}" type="text/javascript"></script>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBEFsQAyB91aAUVhFYqSKS_vBhYbV14qeE&libraries=places&language=frU&callback=initMap" async defer></script>

<!-- template js -->
<script src="{{URL::asset('assets')}}/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<script src="{{URL::asset('assets')}}/js/plugins/chartist.min.js"></script>
<script src="{{URL::asset('assets')}}/js/core/popper.min.js"></script>
<script src="{{URL::asset('assets')}}/js/core/bootstrap-material-design.min.js"></script>
<script src="{{URL::asset('assets')}}/js/material-dashboard.js?v=2.1.1" type="text/javascript"></script>
<!--====== Close JS Files ======-->


<script>
    $(document).ready(function () {
        // Javascript method's body can be found in assets/dist/custom.js
        md.initDashboardPageCharts();
    });
</script>
</body>
</html>
