    <!-- left sidebar -->
<div class="sidebar" data-color="orange" data-background-color="white"
     data-image="{{URL::asset('assets')}}/img/sidebar-1.jpg">
    <div class="logo">
        <a href="{{ route('admin.dashboard') }}" class="simple-text logo-normal">
            {{ __('Plateforme I-tou') }}
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="nav-item @if($status=='Dashboard' || $status=='Profile') {{ 'active' }} @endif">
                <a class="nav-link" href="{{route('admin.dashboard')}}">
                    <i class="material-icons">dashboard</i>
                    <p>{{__('Dashboard')}}</p>
                </a>
            </li>

            @if(Auth::guard('admin')->user()->isAdminGeneral())
                <li class="nav-item @if($status=='Site') {{ 'active' }} @endif">
                    <a class="nav-link" href="{{route('admin.sites.show')}}">
                        <i class="material-icons">web</i>
                        <p>{{__('Site')}}</p>
                    </a>
                </li>

                <li class="nav-item @if($status=='Admins') {{ 'active' }} @endif">
                    <a class="nav-link" href="{{route('admin.admins.show')}}">
                        <i class="material-icons">web</i>
                        <p>{{__('Site Admins')}}</p>
                    </a>
                </li>

                <li class="nav-item @if($status=='Delivery Options') {{ 'active' }} @endif">
                    <a class="nav-link" href="{{route('admin.delivery_options.show')}}">
                        <i class="material-icons">flight_takeoff</i>
                        <p>{{__('Option de livraison')}}</p>
                    </a>
                </li>
            @endif


            <li class="nav-item @if($status=='Users') {{ 'active' }} @endif">
                <a class="nav-link" href="{{route('admin.users.show')}}">
                    <i class="material-icons">people</i>
                    <p>{{__('Utilisateurs')}}</p>
                </a>
            </li>

            <li style="display:none" class="nav-item @if($status=='Addresses') {{ 'active' }} @endif">
                <a class="nav-link" href="{{route('admin.challenge.show')}}">
                    <i class="material-icons">location_on</i>
                    <p>{{__('Adresses')}}</p>
                </a>
            </li>
            <li style="display:none" class="nav-item @if($status=='Transactions') {{ 'active' }} @endif">
                <a class="nav-link" href="{{route('admin.transactions.show')}}">
                    <i class="material-icons">swap_horiz</i>
                    <p>{{__('Transactions')}}</p>
                </a>
            </li>


            <li class="nav-item @if($status=='Orders') {{ 'active' }} @endif">
                <a class="nav-link" href="{{route('admin.orders.show')}}">
                    <i class="material-icons">format_line_spacing</i>
                    <p>{{__('Commandes')}}</p>
                </a>
            </li>


            <li class="nav-item @if($status=='Items') {{ 'active' }} @endif">
                <a class="nav-link" href="{{route('admin.items.show')}}">
                    <i class="material-icons">list</i>
                    <p>{{__('Produits')}}</p>
                </a>
            </li>

            <li class="nav-item @if($status=='Category') {{ 'active' }} @endif">
                <a class="nav-link" href="{{route('admin.categories.show')}}">
                    <i class="material-icons">category</i>
                    <p>{{__('Catégories')}}</p>
                </a>
            </li>


            <li class="nav-item @if($status=='Slider') {{ 'active' }} @endif">
                <a class="nav-link" href="{{route('admin.sliders.show')}}">
                    <i class="material-icons">photo_album</i>
                    <p>{{__('Sliders')}}</p>
                </a>
            </li>

            <li class="nav-item @if($status=='Promotion') {{ 'active' }} @endif">
                <a class="nav-link" href="{{route('admin.promotions.show')}}">
                    <i class="material-icons">poll</i>
                    <p>{{__('Promos')}}</p>
                </a>
            </li>

            <li class="nav-item @if($status=='Newsletter Subscriptions') {{ 'active' }} @endif">
                <a class="nav-link" href="{{route('admin.newsletter.show')}}">
                    <i class="material-icons">email</i>
                    <p>{{__('Newsletter')}}</p>
                </a>
            </li>

            <li class="nav-item @if($status=='Pages') {{ 'active' }} @endif">
                <a class="nav-link" href="{{route('admin.pages.show')}}">
                    <i class="material-icons">email</i>
                    <p>{{__('Pages')}}</p>
                </a>
            </li>


            <li class="nav-item @if($status=='Settings') {{ 'active' }} @endif">
                <a class="nav-link" href="{{route('admin.settings.show')}}">
                    <i class="material-icons">settings</i>
                    <p>{{__('Paramètres')}}</p>
                </a>
            </li>

            <li class="nav-item @if($status=='Message') {{ 'active' }} @endif">
                <a class="nav-link" href="{{route('admin.message.show')}}">
                    <i class="material-icons">settings</i>
                    <p>{{__('Message')}}</p>
                </a>
            </li>
        </ul>
    </div>
</div>
