<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGameEntireVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_entire_videos', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('challenge_id');
            $table->string('game_group');
            $table->string('description')->nullable();
            $table->string('name')->nullable();
            $table->string('file_path')->nullable();
            $table->string('report_id')->nullable();
            $table->string('active_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_entire_videos');
    }
}
