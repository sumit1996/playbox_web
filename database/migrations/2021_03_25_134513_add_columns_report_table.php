<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('game_entry_report', function (Blueprint $table) {
            $table->integer('options')->change();
            $table->renameColumn('options', 'report_1');
            $table->integer('report_2')->nullable();
            $table->integer('report_3')->nullable();
            $table->integer('report_4')->nullable();
            $table->integer('report_5')->nullable();
            $table->integer('report_6')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('game_entry_report', function (Blueprint $table) {
            $table->renameColumn('report_1', 'options');
        });
    }
}
