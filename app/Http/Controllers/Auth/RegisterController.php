<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{

    //    private $http = 'http://myapp_web.in/';
//    private $http = 'http://playbox.webronix.com/';
    private $http = 'https://kreelok.com/';
    private $request;

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            //'remember_me' => 'boolean'
        ]);
        $credentials = request(['email', 'password']);

        if (!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->save();
        $user->api_token = $token;

//        if ($request->remember_me)
//            $token->expires_at = Carbon::now()->addWeeks(1);
        if ($user->save()) {
            return response()->json([
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                'token' => $token,
//            'expires_at' => Carbon::parse(
//                $tokenResult->token->expires_at
//            )->toDateTimeString()
            ]);
        }

    }

    public function register(Request $request)
    {

        $validatedData = $request->validate([
            'firstname' => '',
            'lastname' => '',
            'username' => '',
            'email' => '',
            'phone' => '',
            'about_me' => '',
            'country' => '',
            'city' => '',
            'flag' => '',
            'DOB' => '',
            'gender' => '',
            'password' => '',
            'facebook_id' => '',
            'google_id' => ''
        ]);

        if ($request->facebook_id || $request->google_id) {
            if($request->google_id){
                $validatedData['google_id'] = $request->google_id;
                $validatedData['image'] = $this->http . "image/avtar.jpg";
                $validatedData['email'] = $request->email;
                $validatedData['phone'] = "";
            }else{
                $validatedData['facebook_id'] = $request->facebook_id;
                $validatedData['image'] = $request->picture;
                $validatedData['email'] = strtolower($request->firstname."@facebook.com");
                $validatedData['phone'] = "";
            }
        }else{

            $username_existance = User::where('username', $request->username)->count();
            if ($username_existance > 0) {

                return response([
                    'success' => false,
                    'error' => 'username_exist',
                ]);
            }
            $useremail_existance = User::where('email', $request->email)->count();
            if ($useremail_existance > 0) {

                return response([
                    'success' => false,
                    'error' => 'email_exist',
                ]);
            }
            $validatedData['email'] = strtolower($request->email);
            $validatedData['phone'] = $request->phone;
            $validatedData['image'] = $this->http . "image/avtar.jpg";
        }

        $validatedData['firstname'] = $request->firstname;
        $validatedData['lastname'] = $request->lastname;
        $validatedData['username'] = $request->username;

        $validatedData['password'] = bcrypt($request->password);
        $validatedData['about_me'] = "";
        $validatedData['country'] = "";
        $validatedData['city'] = "";
        $validatedData['flag'] = "";
        $validatedData['DOB'] = "";
        $validatedData['gender'] = "";
        $user = User::create($validatedData);
        $accessToken = $user->createToken('authToken')->accessToken;
        $user->save();


        return response()->json([
            'success' => true,
            'email' => $request->email,
            'message' => 'Successfully created user!',
            'token' => $accessToken
        ], 201);
    }
//    public function register(Request $request)
//    {
//
//
//        $user = new User;
//        $user->firstname = $request->firstname;
//        $user->lastname = $request->lastname;
//        $user->username = $request->username;
//        if ($request->facebook_id) {
//            $user->facebook_id = $request->facebook_id;
//            $user->image = $request->picture;
//            $user->email = strtolower($request->firstname."@facebook.com");
//            $user->phone = "";
//
//        }else{
//
//            $username_existance = User::where('username', $request->username)->count();
//            if ($username_existance > 0) {
//
//                return response([
//                    'success' => false,
//                    'error' => 'username_exist',
//                ]);
//            }
//            $useremail_existance = User::where('email', $request->email)->count();
//            if ($useremail_existance > 0) {
//
//                return response([
//                    'success' => false,
//                    'error' => 'email_exist',
//                ]);
//            }
//
//            $user->email = strtolower($request->email);
//            $user->phone = $request->phone;
//            $user->image = $this->http . "image/avtar.jpg";
//        }
//
//
//        $user->about_me = "";
//        $user->country = "";
//        $user->city = "";
//        $user->flag = "";
//        $user->DOB = "";
//        $user->gender = "";
//
//
//        $user->password = bcrypt($request->password);
////        $accessToken = $user->createToken('Personal Access Token');
//        $accessToken = $user->createToken('Laravel Password Grant Client')->accessToken;
//        $user->api_token = $accessToken;
//        $user->save();
////        $accessToken1 = $user->createToken('authToken')->accessToken;
////        $tokenResult = $user->createToken('Personal Access Token');
////        $token = $tokenResult->accessToken;
//
//        return response()->json([
//            'success' => true,
//            'email' => $request->email,
//            'message' => 'Successfully created user!',
//            'token' => $accessToken
//        ], 201);
//    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }
    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }

}
