<?php

namespace App\Http\Controllers;

use App\Models\Challenge;
use App\Models\prediction;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ChallengeController extends Controller
{
    // Display all challenge
    public function show()
    {

        // Fetch address from challenge table where deleted not equal to null
        $challenge = Challenge::where('deleted_at', null)->orderBy('created_at', 'desc')->get();

        return view('admin.page.challenge.browse')
            ->with('challenges', $challenge)->with('no', 1)->with('status', 'Challenge');
    }

    // Delete address
    public function delete($id)
    {
        // find Challenge and delete
        $Challenge = Challenge::find($id);
        $Challenge->delete();

        return redirect()->route('challenge.show');

    }


    // Display Edit Challenge Page
    public function edit($id)
    {
        $challenge = Challenge::where('id', $id)->where('deleted_at', null)->firstOrFail();
        $users = User::all();
        return view('admin.page.challenge.edit' , compact('challenge', 'users'))
            ->with('status', 'Challenge');
    }


    // Display Add Challenge Page
    public function add()
    {
        $users = User::all();
        return view('admin.page.challenge.add', compact( 'users'))
            ->with('status', 'Challenge');
    }


    // Add and Update Challenge
    public function update(Request $request)
    {
//        dd($request->image);
//
        // validation rules.
        $rules = [
            'challenge_name'     => 'required|string',
            'title'     => 'required|string',
        ];

        //custom validation error messages.
        $messages = [
//            'name.unique' => 'Address name already exist in our records.',
        ];

        //validate the request.
        if ($request->validate($rules, $messages)) {

            if ($request->id) { // for edit

                $challenge = Challenge::find($request->id);
            } else { // for add

                $challenge          = new Challenge();
            }

            // for both
            $challenge->challenge_name     = $request->challenge_name;
            $challenge->title     = $request->title;
            $challenge->description   = $request->description;
            $challenge->hashtag = $request->hashtag;
            $challenge->challenge_type_video = $request->challenge_type_video;
            $challenge->challenge_type_picture = $request->challenge_type_picture;
            $challenge->challenge_type_prediction = $request->challenge_type_prediction;
//            dd($request->hasFile('image'));
            if ($request->hasFile('image')) {
                $image    = $request->file('image');
                $fileName = 'challenge' . '_' . time() . '.' . $image->getClientOriginalExtension();
                Storage::disk('uploads')->put('images/challenge/' . $fileName, File($image));
                $filepath        = 'uploads/images/challenge/' . $fileName;

                $challenge->image = $filepath;
            }else{
                $challenge->image = $challenge->image;
            }

            if ($request->hasFile('thumbnail')) {
                $image    = $request->file('thumbnail');
                $fileName = 'challenge' . '_' . time() . '.' . $image->getClientOriginalExtension();
                Storage::disk('uploads')->put('images/challenge/' . $fileName, File($image));
                $filepath        = 'uploads/images/challenge/' . $fileName;

                $challenge->thumbnail = $filepath;
            }else{
                $challenge->thumbnail = $challenge->thumbnail;
            }

            $challenge->active  = $request->active;
            $challenge->publish_by      = $request->publish_by;
            $challenge->points      = $request->points;
            $challenge->team_first      = $request->team_first;
            $challenge->team_second      = $request->team_second;

            if ($challenge->save()) {

                return redirect()->intended(route('challenge.show'))->with('status', 'challenges');
            }

        } else {

            if ($request->id) { // for edit

                return redirect()->route('challenge.edit', ['id' => $request->id])->with('status', 'Challenges');
            } else { // for add

                return redirect()->back()->with('status', 'Challenges');
            }

        }


    }


    public function prediction(){
        // Fetch prediction
        $challenge = Challenge::where('challenge_type_prediction', 1)->where('deleted_at', null)->orderBy('created_at', 'desc')->get();

        return view('admin.page.prediction.browse')
            ->with('challenges', $challenge)->with('no', 1)->with('status', 'Prediction Challenge');
    }

    public function prediction_edit($id){
        $challenge = Challenge::where('challenge_type_prediction', 1)->where('id', $id)->where('deleted_at', null)->firstOrFail();
        $users = User::all();
        $predictions = prediction::where('challenge_id', $id)->first();
        return view('admin.page.prediction.edit' , compact('challenge', 'users', 'predictions'))
            ->with('status', 'Challenge');
    }


    // update prediction challenge app data
    public function prediction_update(Request $request){
//        $count = prediction::where('challenge_id', $request->id)->count();


        $prediction_challenge = DB::table('predictions')
            ->where("challenge_id",$request->id)
            ->update(array('show_status' => $request->show_status , 'answer' => $request->answer , 'answer1' => $request->answer1));

            if($request->show_status == "stop"){
                DB::table('challenges')
                    ->where("id",$request->id)
                    ->update(array('stop' => 1));
            }else{
                DB::table('challenges')
                    ->where("id",$request->id)
                    ->update(array('stop' => 2));
            }

//        echo $prediction_challenge;
        if ($prediction_challenge) {
            return redirect()->intended(route('prediction'))->with('status', 'prediction');
        }

    }
}
