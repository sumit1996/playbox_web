<?php

namespace App\Http\Controllers;

use App\Helper\Helper;
use App\Models\relationship;
use App\Models\User;
use Illuminate\Http\Request;

class FriendController extends Controller
{
    //    private $http = 'http://myapp_web.in/';
//    private $http = 'http://playbox.webronix.com/';
    private $http = 'https://kreelok.com/';
    private $request;

    /**
     * send friend request
     */

    public function friend_reuest(Request $request)
    {
        if ($request->first_user_id != "" && $request->second_user_id != "") {
            $relationship = new relationship;
            $relationship->first_user_id = $request->first_user_id;
            $relationship->second_user_id = $request->second_user_id;
            $relationship->status = 1;
            $relationship->action_user_id = $request->first_user_id;
            if ($relationship->save()) {
                Helper::notification($request->second_user_id, "null", "You have a new friend request");
                return response([
                    'success' => true,
//                    'request' => Helper::notification($request->second_user_id, "null", "You have a new friend request"),
                    'relationship' => $relationship
                ]);
            }

        } else {
            return response([
                'success' => false
            ]);
        }
    }

    /**
     * get all pending request
     */

    public function get_friend_request(Request $request)
    {
        $friend_request_data = array();
        $send_friend_request_data = array();
        $friends_data = array();
        if ($request->user_id != "") {
            $friend_request = relationship::where('second_user_id', $request->user_id)
                ->where('status', 1)
                ->where('action_user_id', '!=', $request->user_id)->get();
            foreach ($friend_request as $key => $friend) {
                $users = User::where('id', $friend->first_user_id)->first();
                $friend_request_data[$key] = $users;
            }
            $send_friend_request = relationship::where('first_user_id', $request->user_id)
                ->where('status', 1)
                ->where('action_user_id', $request->user_id)->get();
            foreach ($send_friend_request as $key => $friend) {
                $users = User::where('id', $friend->second_user_id)->first();
                $send_friend_request_data[$key] = $users;
            }

//            $friends = relationship::where('first_user_id', $request->user_id)
//                ->orWhere('second_user_id', $request->user_id)
//                ->where('status', 2)
//                ->get();
            $friend1 =  relationship::where('first_user_id', $request->user_id)
                ->where('status', 2)
                ->get();
            $friend2 =  relationship::where('second_user_id', $request->user_id)
                ->where('status', 2)
                ->get();
//            $friends = array_merge($friend1 , $friend2);
            if (!empty($friend1) && !empty($friend2)) {
                $friends = $friend1->merge($friend2);
            }

            foreach ($friends as $key => $friend) {
                if($request->user_id != $friend->second_user_id){
                    $user_id = $friend->second_user_id;
                }elseif ($request->user_id != $friend->first_user_id){
                    $user_id = $friend->first_user_id;
                }
                $users = User::where('id', $user_id)->first();
                $friends_data[$key] = $users;
            }
            return response([
                'success' => true,
                'request' => $request->all(),
                'friend_request' => $friend_request,
                'friend_request_data' => $friend_request_data,
                'send_friend_request' => $send_friend_request,
                'send_friend_request_data' => $send_friend_request_data,
                'friends_data' => $friends_data
            ]);


        } else {
            return response([
                'success' => false
            ]);
        }
    }

    /**
     * accept friend request
     */

    public function accept_friend_request(Request $request)
    {
        if ($request->first_user_id != "" && $request->second_user_id != "") {
            $relationship_accept = relationship::where('first_user_id', $request->second_user_id)
                ->where('second_user_id', $request->first_user_id)
                ->limit(1)
                ->update([
                    'status' => 2,
                    'action_user_id' => $request->first_user_id
                ]);
            Helper::notification($request->second_user_id, "null", "your friend request is accept");
            return response([
                'success' => true,
                'request' => $request,
                'relationship_accept' => $relationship_accept
            ]);

        } else {
            return response([
                'success' => false
            ]);
        }
    }
}
