<?php

namespace App\Http\Controllers;

use App\Helper\Helper;
use App\Models\Challenge;
use App\Models\game_entire_video;
use App\Models\game_entrie;
use App\Models\game_entry_comment;
use App\Models\game_entry_report;
use App\Models\GameEntryLike;
use App\Models\MyGame;
use App\Models\Point;
use App\Models\prediction;
use App\Models\prediction_point;
use App\Models\relationship;
use App\Models\User;
use App\Models\UsersPlayerId;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use PragmaRX\Countries\Package\Countries;

class MainController extends Controller
{
//    private $http = 'http://myapp_web.in/';
//    private $http = 'http://playbox.webronix.com/';
    private $http = 'https://kreelok.com/';
    private $request;

    /**
     * get all the challenges
     */
    public function all_challenges()
    {
        $all_challenges = array();
        $all_challenge = DB::table('challenges')->where('active', 1)->orderBy('created_at', 'desc')->get();
        if ($all_challenge) {
            foreach ($all_challenge as $key => $challenges) {
                $title = $challenges->title;
                $challenge_name = $challenges->challenge_name;
                $description = $challenges->description;
                $hashtag = $challenges->hashtag;
                $id = $challenges->id;
                $image =$this->http .  $challenges->image;
                $thumbnail = $this->http . $challenges->thumbnail;
                $points = $challenges->points;
                $publish_by = $challenges->publish_by;
                $challenge_type = null;
                $challenge_type_video = $challenges->challenge_type_video;
                $challenge_type_picture = $challenges->challenge_type_picture;
                $challenge_type_prediction = $challenges->challenge_type_prediction;
                if ($challenge_type_video > 0) {
                    $challenge_type = "video";
                } elseif ($challenge_type_picture > 0) {
                    $challenge_type = "picture";
                } elseif ($challenge_type_prediction > 0) {
                    $challenge_type = "prediction";

                }

                $user_publish_by = User::where('username', $publish_by)->first();


                $all_challenges[$key] = array(
                    "img" => $image,
                    "thumbnail" => $thumbnail,
                    "title" => $title,
                    "challenge_name" => $challenge_name,
                    "description" => $description,
                    'hashtag' => $hashtag,
                    "id" => $id,
                    "points" => $points,
                    'publish_by' => $publish_by,
                    'challenge_type' => $challenge_type,
                    'user_publish_by' => $user_publish_by
                );
            }

        }
        return response([
            'success' => true,
            'all_challenges' => $all_challenges
        ]);
    }


    /**
     * get all the data according the game challenge.
     */
    public function get_challenge_data(Request $request)
    {

        $challenge = array();
        $challenge = DB::table('challenges')->where('active', 1)->where('id', $request->challenge_id_group)->get();

        if ($challenge) {
            foreach ($challenge as $key => $challenges) {
                $title = $challenges->title;
                $description = $challenges->description;
                $hashtag = $challenges->hashtag;
                $id = $challenges->id;
                $image = $this->http . $challenges->image;
                $points = $challenges->points;
                $publish_by = $challenges->publish_by;


                $challenge = array(
                    "img" => $image,
                    "title" => $title,
                    "description" => $description,
                    'hashtag' => $hashtag,
                    "id" => $id,
                    "points" => $points,
                    'publish_by' => $publish_by
                );
            }
            $group_entry_id = "";
            $group_entry_d = game_entrie::where('challenge_id', $request->challenge_id_group)->where('user_id', $request->user_id)->first();
            if (isset($group_entry_d) && $group_entry_d != null) {
                $group_entry_id = $group_entry_d->id;
            }

            return response([
                'success' => true,
                'challenge' => $challenge,
                'group_entry_id' => $group_entry_id,
            ]);
        }

        return response([
            'success' => false
        ]);

    }

    public function get_all_entry(Request $request)
    {

        $group_entry_all_data = array();
        if (isset($request->challenge_id_group) && $request->challenge_id_group != null) {
            $group_entry_top = game_entrie::with('user')->where('challenge_id', $request->challenge_id_group)->inRandomOrder()->get();
            $key = 0;
            foreach ($group_entry_top as  $value) {
                $GameEntryLikeData = Helper::get_like_count_data($value->id, $value->challenge_id, $value->user_id);
                $GameEntryLikeDataIcon = Helper::get_like_count_data_icon($value->id, $value->challenge_id, $request->user_id);
                $GameEntryCommentData = Helper::get_comment_count_data($value->id, $value->challenge_id, $value->user_id);

                if ($GameEntryLikeData > 10) {
                    $group_entry_all_data[$key] = array(
                        "GameEntryLikeData" => $GameEntryLikeData,
                        "GameEntryCommentData" => $GameEntryCommentData,
                        "GameEntryLikeDataIcon" => $GameEntryLikeDataIcon === 0 ? "outline" : "sharp",
                        "challenge_id" => $value->challenge_id,
                        "active_id" => $value->active_id,
                        'description' => $value->description,
                        "game_group" => $value->game_group,
                        "id" => $value->id,
                        'image' => $value->image,
                        'report_id' => $value->report_id,
                        'user_id' => $value->user_id,
                        'group_entry' => $value
                    );
                    $key++;
                }

            }
            return response([
                'success' => true,
                'group_entry_all_data' => $group_entry_all_data
            ]);
        }
        return response([
            'success' => false,
            'request' => $request->all(),
//            'latest_entry' => $group_entry_latest
        ]);

    }


    /**
     * save the game entries in the database.
     */
    public function game_entry_data(Request $request)
    {
        $game_entries_count = game_entrie::where('challenge_id', $request->challenge_id_camera)->count();
        $game_entrie = new game_entrie();
        $game_entrie->challenge_id = $request->challenge_id_camera;
        $game_entrie->description = $request->gameDescription;
        $game_entrie->image = $request->gameImage;
        $game_entrie->user_id = $request->user_id;
        if ($game_entries_count % 20 == 0) {
            $k = $game_entries_count / 20;
        }
        if (isset($k)) {
            $game_entrie->game_group = 'group_of_people' . $k . '_' . $request->challenge_id_camera;
        } else {
            $last_entry = DB::table('game_entries')->where('challenge_id', $request->challenge_id_camera)->latest('updated_at')->first();
            $game_entrie->game_group = $last_entry->game_group;
        }
        $game_entrie->report_id = "";
        $game_entrie->active_id = "";
        if ($game_entrie->save()) {
            Helper::save_points($request->user_id, $request->point);
            return response([
                'success' => true,
                'count' => $game_entries_count,
                'challenge_id_camera' => $request->challenge_id_camera,
//                'k' => $k,
                'id' => $game_entrie->id
            ]);
        }

        return response([
            'success' => false,
            'count' => $game_entries_count
        ]);

    }

    /**
     * get all the group entry
     */
    public function get_all_group_entry(Request $request)
    {

        $get_group_entry = game_entrie::where('id', $request->group_game_id)->first();
        $GameEntryLikeCount = array();
        if (isset($get_group_entry) && $get_group_entry != null) {
            $get_all_group_entry = game_entrie::with('user')->where('game_group', $get_group_entry->game_group)->get();

            foreach ($get_all_group_entry as $key => $value) {
                $GameEntryLikeData = Helper::get_like_count_data($value->id, $value->challenge_id, $value->user_id);
                $GameEntryLikeDataIcon = Helper::get_like_count_data_icon($value->id, $value->challenge_id, $request->user_id);

                $GameEntryCommentData = Helper::get_comment_count_data($value->id, $value->challenge_id, $value->user_id);
                $GameEntryLikeCount[$key] = array(
                    "GameEntryLikeData" => $GameEntryLikeData,
                    "GameEntryLikeDataIcon" => $GameEntryLikeDataIcon === 0 ? "outline" : "sharp",
                    "GameEntryCommentData" => $GameEntryCommentData,
                    "challenge_id" => $value->challenge_id,
                    "active_id" => $value->active_id,
                    'description' => $value->description,
                    "game_group" => $value->game_group,
                    "id" => $value->id,
                    'image' => $value->image,
                    'report_id' => $value->report_id,
                    'user_id' => $value->user_id,
                    'group_entry' => $value,
                    'user_data' => Helper::get_user_data($value->user_id)
                );

            }
            return response([
                'success' => true,
                'all_group_entry' => $GameEntryLikeCount,
                'game_group' => $get_group_entry,
                'id' => $request->group_game_id
            ]);
        }
        return response([
            'success' => false,
            'get_group_entry' => $get_group_entry,
            'id' => $request->group_game_id
        ]);
    }

    /**
     * save the likes of each game entry
     */
    public function game_entry_like(Request $request)
    {
        $GameEntryLikeCount = GameEntryLike::where('game_entry_id', $request->group_entry_id_for_like)
            ->where('challenge_id', $request->challenge_id)
            ->where('user_id', $request->user_id)
            ->count();
        if ($GameEntryLikeCount > 0) {

            $GameEntryLike = GameEntryLike::where('game_entry_id', $request->group_entry_id_for_like)
                ->where('user_id', $request->user_id)
                ->where('challenge_id', $request->challenge_id);
            if ($GameEntryLike->delete()) {
                return response([
                    'success' => true,
                    'value' => 'delete_row',
                    'game_entry_like' => $request->all()
                ]);
            }
        } else {

            $GameEntryLike = new GameEntryLike();
            $GameEntryLike->game_entry_id = $request->group_entry_id_for_like;
            $GameEntryLike->user_id = $request->user_id;
            $GameEntryLike->challenge_id = $request->challenge_id;
            $GameEntryLike->like = 1;
            if ($GameEntryLike->save()) {
                return response([
                    'success' => true,
                    'value' => 'create',
                    'game_entry_like' => $request->all()
                ]);
            }
        }

    }

    /**
     * user update profile
     */
    public function update_profile(Request $request)
    {
//        $flag_url = 'https://lipis.github.io/flag-icon-css/flags/4x3/' . strtolower($request->country) . '.svg';
        $flag_url = 'https://ipdata.co/flags/' . strtolower($request->country) . '.png';

        if ($user = User::find($request->id)) {
            $user->firstname = $request->firstname;
            $user->lastname = $request->lastname;
            $user->username = $request->username;
            $user->email = $request->email;
            $user->phone = $request->phone;

            $user->about_me = $request->about_me;
            $user->country = $request->country;
            $user->city = $request->city;
            $user->flag = $flag_url;
            $user->DOB = $request->dob;
            $user->gender = $request->gender;

//        $user->password = bcrypt($request->password);
//        $accessToken = $user->createToken('authToken')->accessToken;
//        $user->remember_token = $accessToken;
            $user->save();
            return response([
                'success' => true,
                'done' => 'update',
                'value' => $request->all(),
                'user' => $user
            ]);
        }
        return response([
            'success' => false,
            'done' => 'update-not',
            'value' => $request->all()
        ]);
    }

    /**
     * save user profile image
     */
    public function user_image_save(Request $request)
    {

        if ($user = User::find($request->user_id)) {
            $user->image = $request->UserImage;
            $user->save();
            return response([
                'success' => true,
                'done' => 'update',
                'value' => $request->all(),
                'user' => $user
            ]);
        }
        return response([
            'success' => false,
            'done' => 'update-not',
            'value' => $request->all()
        ]);
    }

    /**
     * save my game
     */
    public function save_my_game(Request $request)
    {
        $mygamecount = DB::table('mygames')->where('user_id', $request->user_id)->where('challenge_id', $request->challenge_id)->count();
        if ($mygamecount <= 0) {
            $mygame = new MyGame;
            $mygame->user_id = $request->user_id;
            $mygame->challenge_id = $request->challenge_id;
            $mygame->status = $request->status;
            $mygame->save();
            return response([
                'success' => true,
                'done' => 'create',
                'value' => $request->all(),
                'mygame' => $mygame
            ]);
        } else {
            if ($request->status != 'pending') {

                $mygame = DB::table('mygames')->where('user_id', $request->user_id)
                    ->where('challenge_id', $request->challenge_id)
                    ->update(['status' => $request->status]);
                return response([
                    'success' => true,
                    'done' => 'update',
                    'value' => $request->all(),
                    'mygame' => $mygame
                ]);
            }
        }

    }

    /**
     * get my game by user id
     */
    public function get_my_game(Request $request)
    {
//        Helper::notification(82, null, "your friend request is accept");
        $all_challenges_my_game = array();
        $thumbnail_image = "";
        $mygames = DB::table('mygames')->where('user_id', $request->user_id)->get();
        if (!empty($mygames)) {
            foreach ($mygames as $key => $mygame) {
                $challenge = DB::table('challenges')->where('active', 1)
                    ->where('id', $mygame->challenge_id)->first();
                if ($challenge->challenge_type_video > 0) {
                    $challenge_type = "video";
                    $thumbnail_image = $this->http . $challenge->thumbnail;
                } elseif ($challenge->challenge_type_picture > 0) {
                    $challenge_type = "picture";
                } elseif ($challenge->challenge_type_prediction > 0) {
                    $challenge_type = "prediction";
                }
                $all_challenges_my_game[$key] = array(
                    "img" => $this->http . $challenge->image,
                    "thumbnail" => $thumbnail_image,
                    "title" => $challenge->title,
                    "challenge_name" => $challenge->challenge_name,
                    "description" => $challenge->description,
                    'hashtag' => $challenge->hashtag,
                    "id" => $challenge->id,
                    "points" => $challenge->points,
                    'publish_by' => $challenge->publish_by,
                    'challenge_type' => $challenge_type,
                    'user_publish_by' => $challenge->image,
                    'status' => $mygame->status,
                    'mygameid' => $mygame->id
                );
            }


            return response([
                'success' => true,
                'value' => $request->all(),
                'mygame' => $mygames,
                'close_img' => $this->http . 'image/colse.jpg',
                'thumbnail' => $this->http . 'image/default-video-thumbnail.jpg',
                'all_challenges_my_game' => $all_challenges_my_game
            ]);
        }
        return response([
            'success' => false,
            'value' => $request->all(),
            'mygame' => $mygames
        ]);
    }

    /**
     * remove my game by user id
     */
    public function remove_my_game(Request $request)
    {
        $mygames = DB::table('mygames')->where('id', $request->mygameid);
        if ($mygames->delete()) {
            return response([
                'success' => true,
                'value' => 'delete_mygame',
                'mygame' => $request->all()
            ]);
        }
        return response([
            'success' => false,
            'value' => 'not_delete_mygame',
            'mygame' => $request->all()
        ]);
    }

    /**
     * save reports
     */
    public function game_entry_report(Request $request)
    {
        $game_entry_reportcount = DB::table('game_entry_report')
            ->where('user_id', $request->user_id)
            ->where('challenge_id', $request->challenge_id_report)
            ->where('game_entry_id', $request->group_entry_id_for_report)
            ->count();

        if ($game_entry_reportcount > 0) {
            return response([
                'success' => true,
                'status' => 'update',
                'value' => $request->all()
            ]);
        } else {
            $game_entry_report_count = DB::table('game_entry_report')
                ->where('challenge_id', $request->challenge_id_report)
                ->where('game_entry_id', $request->group_entry_id_for_report)
                ->where('game_entry_user_id', $request->game_entry_user_id)
                ->count();

            if ($game_entry_report_count > 0) {

                Helper::report_after_10_people($request->game_entry_user_id, $request->challenge_id_report, $request->group_entry_id_for_report, $request->challengename_report);

                return response([
                    'success' => true,
                    'status' => 'report',
                    'report_after_10_people' => Helper::report_after_10_people($request->user_id, $request->challenge_id_report, $request->group_entry_id_for_report, $request->challengename_report)
                ]);
            } else {
                $game_entry_report = new game_entry_report;
                $game_entry_report->user_id = $request->user_id;
                $game_entry_report->game_entry_user_id = $request->game_entry_user_id;
                $game_entry_report->challenge_id = $request->challenge_id_report;
                $game_entry_report->game_entry_id = $request->group_entry_id_for_report;
                $game_entry_report->description = $request->report_text;
                $game_entry_report->report_1 = $request->report_1;
                $game_entry_report->report_2 = $request->report_2;
                $game_entry_report->report_3 = $request->report_3;
                $game_entry_report->report_4 = $request->report_4;
                $game_entry_report->report_5 = $request->report_5;
                $game_entry_report->report_6 = $request->report_6;
                $game_entry_report->save();
                return response([
                    'success' => true,
                    'status' => 'create',
                    'value' => $request->all(),
                    'game_entry_report' => $game_entry_report,
                    'game_entry_reportcount' => $game_entry_reportcount
                ]);
            }
        }

    }

    /**
     * get the user data
     */
    public function userInformation(Request $request)
    {
        if ($user = $request->User()) {
            return response()->json([
                'success' => true,
                'userData' => $user
            ]);
        }
    }

    /**
     * get the search result
     */
    public function all_users(Request $request)
    {
        if ($request->search_text != "" && $request->user_id != "") {
            $all_users = User::where([
                    ['id', '!=', $request->user_id],
                    ['firstname', 'LIKE', "%$request->search_text%"]
                ])->orWhere('username', 'LIKE', "%$request->search_text%")->get();

            return response([
                'success' => true,
                'search_text' => $request->search_text,
                'user_id' => $request->user_id,
                'all_users' => $all_users
            ]);
        } else {
            return response([
                'success' => false
            ]);
        }

    }

    /**
     * get friend data by user id
     */

    public function get_friends(Request $request)
    {
        $status = "";
        $merge = array();
        $user_status = "";
        $friend1 = "";
        $friend2 = "";
        if ($request->friend_user_id != "") {
            $users = User::where('id', $request->friend_user_id)->first();
            if ($request->friend_request_status == 'friend_request') {

                $user_status = relationship::where('first_user_id', $request->friend_user_id)->
                where('second_user_id', $request->user_id)->first();

            } elseif ($request->friend_request_status == 'send_request') {
                $user_status = relationship::where('first_user_id', $request->user_id)->
                where('second_user_id', $request->friend_user_id)->first();
            } elseif ($request->friend_request_status == 'search') {

                $user_status1 = relationship::where('first_user_id', $request->friend_user_id)->
                where('second_user_id', $request->user_id)->first();
                $user_status2 = relationship::where('first_user_id', $request->user_id)->
                where('second_user_id', $request->friend_user_id)->first();
                if (!empty($user_status1)) {
                    $user_status = $user_status1;
                }
                if (!empty($user_status2)) {
                    $user_status = $user_status2;
                }

            } else {

//                $user_status = relationship::where('first_user_id', $request->user_id)->
//                orWhere('second_user_id', $request->user_id)->first();
//                $friend1 = relationship::where('first_user_id', $request->user_id)
//                    ->where('status', 2)
//                    ->first();
//                $friend2 = relationship::where('second_user_id', $request->user_id)
//                    ->where('status', 2)
//                    ->first();
//                $friend2 = relationship::where(function ($query) use ($request) {
//                    $query->where('first_user_id', $request->user_id)
//                        ->where('status', 2)
//                        ->orWhere('second_user_id',$request->user_id)->where;
//                })->get();
                $friend2 = relationship::where('first_user_id', $request->user_id)
                    ->where('second_user_id', $request->friend_user_id)
                    ->where('status', 2)->first();
                if (!empty($friend2)) {
                    $user_status = $friend2;
                }
                $friend1 = relationship::where('first_user_id', $request->friend_user_id)
                    ->where('second_user_id', $request->user_id)
                    ->where('status', 2)->first();

                if (!empty($friend1)) {
                    $user_status = $friend1;
                }

            }
            if (!empty($user_status)) {
                if ($user_status->status == 1 && $user_status->action_user_id == $request->user_id) {
                    $status = "friend_request_sent";
                } elseif ($user_status->status == 1 && $user_status->action_user_id == $request->friend_user_id) {
                    $status = "accept_friend_request";
                } elseif ($user_status->status == 2) {
                    $status = "friends";
                }
            }
            return response([
                'success' => true,
                'request' => $request->all(),
                'status' => $user_status,
                'users' => $users,
                'friend1' => $friend1,
                'friend2' => $friend2,
                'user_status' => $status === '' ? null : $status
            ]);
        } else {
            return response([
                'success' => false
            ]);
        }
    }

    /**
     * save the comment of each game entry
     */
    public function game_entry_comment(Request $request)
    {

        $game_entry_comment = new game_entry_comment();
        $game_entry_comment->game_entry_id = $request->group_entry_id_for_comment;
        $game_entry_comment->user_id = $request->user_id;
        $game_entry_comment->challenge_id = $request->challenge_id;
        $game_entry_comment->comment = $request->comment;
        if ($game_entry_comment->save()) {
            return response([
                'success' => true,
                'value' => 'create',
                '$game_entry_comment' => $request->all()
            ]);
        }


    }

    /**
     * get user all the comment
     */
    public function gat_all_comment(Request $request)
    {
        $userDataComment = array();
        if ($request->group_entry_id_for_comment != "") {
            $gat_all_comments = game_entry_comment::where('game_entry_id', $request->group_entry_id_for_comment)->get();
            foreach ($gat_all_comments as $key => $gat_all_comment) {
                $userData = User::where('id', $gat_all_comment->user_id)->first();
                $userDataComment[$key] = array(
                    "img" => $userData->image,
                    "firstname" => $userData->firstname,
                    "lastname" => $userData->lastname,
                    "date" => $userData->created_at,
                    "comment" => $gat_all_comment->comment
                );
            }

            return response([
                'success' => true,
//                'userData' => $userData,
                'gat_all_comment' => $userDataComment,
                'request' => $request->all()
            ]);
        }
        return response([
            'success' => false,
            'request' => $request->all()
        ]);

    }

    /**
     * save the prediction of each challenge
     */
    public function save_prediction_data(Request $request)
    {
        $prediction_count = DB::table('predictions')->where('challenge_id', $request->challenge_id)->where('user_id', $request->user_id)->count();
        if ($prediction_count > 0) {
            $prediction = DB::table('predictions')->where('user_id', $request->user_id)
                ->where('challenge_id', $request->challenge_id)
                ->update(['predict_text' => $request->predict_text_1, 'predict_text_2' => $request->predict_text_2]);
            return response([
                'success' => true,
                'value' => 'update',
                'game_entry_comment' => $request->all()
            ]);
        } else {
            $prediction = new prediction();
            $prediction->challenge_id = $request->challenge_id;
            $prediction->user_id = $request->user_id;
            $prediction->predict_text = $request->predict_text_1;
            $prediction->predict_text_2 = $request->predict_text_2;
            if ($prediction->save()) {
                return response([
                    'success' => true,
                    'value' => 'create',
                    'game_entry_comment' => $request->all()
                ]);
            }
        }
        return response([
            'success' => false,
            'value' => 'something wrong'
        ]);
    }

    /**
     * get all the data according the prediction game challenge.
     */
    public function gat_all_prediction(Request $request)
    {

        $challenge = array();
        $challenge = DB::table('challenges')->where('active', 1)->where('id', $request->challenge_id)->get();
        if ($challenge) {
            foreach ($challenge as $key => $challenges) {
                $title = $challenges->title;
                $description = $challenges->description;
                $hashtag = $challenges->hashtag;
                $id = $challenges->id;
                $image = $this->http . $challenges->image;
                $points = $challenges->points;
                $publish_by = $challenges->publish_by;
                $team_first = $challenges->team_first;
                $team_second = $challenges->team_second;
                $stop = $challenges->stop;

                $challenge = array(
                    "img" => $image,
                    "title" => $title,
                    "description" => $description,
                    'hashtag' => $hashtag,
                    "id" => $id,
                    "stop_status" => $stop,
                    "points" => $points,
                    'publish_by' => $publish_by,
                    'team_first' => strtoupper($team_first),
                    'team_second' => strtoupper($team_second)
                );
            }
        }
        $userDataPrediction = array();
        if ($request->challenge_id != "" && $request->user_id != "") {
            $predictions = prediction::where('user_id', $request->user_id)->where('challenge_id', $request->challenge_id)->first();
            if (!empty($predictions)) {
                $userData = User::where('id', $predictions->user_id)->first();
                $show_status = "";
                $result = "";
                if($predictions->show_status != ''){
                    if($predictions->show_status == 'stop'){
                        $show_status = 'stop_prediction';
                    }else{
                        $show_status = 'final_prediction';
                        if(((int)$predictions->predict_text == (int)$predictions->answer) && (int)$predictions->predict_text_2 == (int)$predictions->answer1){
                            $prediction_points_count = prediction_point::where('user_id', $request->user_id)->where('challenge_id', $request->challenge_id)->count();
                                if($prediction_points_count > 0){
                                    $result = "Winner";
                                }else{
                                    $prediction_point = new prediction_point();
                                    $prediction_point->user_id = $request->user_id;
                                    $prediction_point->challenge_id = $request->challenge_id;
                                    $prediction_point->points = $challenges->points;
                                    $prediction_point->save();
                                    Helper::save_points($predictions->user_id, $challenges->points);
                                    $result = "Winner";
                                }
                            //

                        }else{
                            $result = "Loser";
                        }
                    }
                }

                $userDataPrediction = array(
                    "img" => $userData->image,
                    "firstname" => $userData->firstname,
                    "lastname" => $userData->lastname,
                    "date" => $userData->created_at,
                    "predict_text" => $predictions->predict_text,
                    "predict_text_2" => $predictions->predict_text_2,
                    "show_status" => $show_status,
                    "result" => $result
                );
            }

            return response([
                'success' => true,
                'challenge' => $challenge,
                'predictions' => $predictions,
                'userDataPrediction' => $userDataPrediction,
                'request' => $request->all()
            ]);

        }
        return response([
            'success' => false,
            'request' => $request->all()
        ]);

    }

    /**
     * get the point by user id.
     */
    public function get_points(Request $request)
    {
        if (isset($request->user_id) && $request->user_id != "") {
            $points = Point::select('points')->where('user_id', $request->user_id)->first();
            return response([
                'success' => true,
                'points' => $points->points
            ]);
        }
        return response([
            'success' => false
        ]);

    }

    /**
     * reset the password
     */
    public function reset_password(Request $request)
    {

        if ($request->email) {

            $password = $this->createRandomPassword();
            $email = $request->email;

            // set this password to user account
            $user = User::where('email', $email)->first();

            if ($user) {
                $user = User::find($user->id);
                $user->password = bcrypt($password);

                if ($user->save()) {

                    $data['email'] = $user->email;
                    $data['password'] = $password;


//                    Mail::send('emails.reset-password', ['data' => $data], function ($message) use ($email) {
//                        $message->to($email)->subject('Dramapasion - Reset Password');
//                    });

                    return response([
                        'success' => true,
                        'data' => $data
                    ]);
                }

            }

        }

        return response([
            'success' => false,
        ]);
    }

    /**
     * create Random Password
     */
    public function createRandomPassword()
    {
        $chars = "abcdefghijkmnopqrstuvwxyz023456789";
        srand((double)microtime() * 1000000);
        $i = 0;
        $pass = '';
        while ($i <= 7) {
            $num = rand() % 33;
            $tmp = substr($chars, $num, 1);
            $pass = $pass . $tmp;
            $i++;
        }
        return $pass;
    }

    /**
     * get world ranking according %
     */
    public function all_world_ranking(Request $request)
    {
        if ($request->filter == "Legend") {
            // get top 5 % data
            $percentage = 5;
            $totalCount = DB::table('points')->count();
            $new_width = ($percentage / 100) * $totalCount;
            $ranking_Top_5 = Point::with('user')->take($new_width)->orderBy('points', 'DESC')->get();
            return response([
                'success' => true,
                'ranking_Top_5' => $ranking_Top_5
            ]);
        } elseif ($request->filter == "Premium") {
            // get 20 % data
            $percentage = 5;
            $totalCount = DB::table('points')->count();
            $new_width = ($percentage / 100) * $totalCount;


            $id = Point::take($new_width)->select('id')->orderBy('points', 'DESC')->get();
            $percentage1 = 20;
            $totalCount1 = Point::whereNotIn('id', $id)->orderBy('points', 'DESC')->count();
            $new_width1 = ($percentage1 / 100) * $totalCount1;
            $ranking_Second_20 = Point::with('user')->take($new_width1)->whereNotIn('id', $id)->orderBy('points', 'DESC')->get();
            return response([
                'success' => true,
                'ranking_Second_20' => $ranking_Second_20
            ]);
        } elseif ($request->filter == "Gold") {
            // get 35 % data
            $percentage = 5;
            $totalCount = DB::table('points')->count();
            $new_width = ($percentage / 100) * $totalCount;


            $id = Point::take($new_width)->select('id')->orderBy('points', 'DESC')->get();
            $percentage1 = 20;
            $totalCount1 = Point::whereNotIn('id', $id)->orderBy('points', 'DESC')->count();
            $new_width1 = ($percentage1 / 100) * $totalCount1;


            $id_20 = Point::take($new_width1)->select('id')->whereNotIn('id', $id)->orderBy('points', 'DESC')->get();
            $id_array = $id->toArray();
            $id_20_array = $id_20->toArray();
            $data_id = array_merge($id_array, $id_20_array);
            $percentage2 = 15;
            $totalCount2 = Point::whereNotIn('id', $data_id)->orderBy('points', 'DESC')->count();
            $new_width2 = ($percentage2 / 100) * $totalCount2;
            $ranking_third_35 = Point::with('user')->take($new_width2)->whereNotIn('id', $data_id)->orderBy('points', 'DESC')->get();
            return response([
                'success' => true,
                'ranking_third_35' => $ranking_third_35
            ]);
        } elseif ($request->filter == "No Batch") {

            // get 40 % data
            $percentage = 5;
            $totalCount = DB::table('points')->count();
            $new_width = ($percentage / 100) * $totalCount;


            $id = Point::take($new_width)->select('id')->orderBy('points', 'DESC')->get();
            $percentage1 = 20;
            $totalCount1 = Point::whereNotIn('id', $id)->orderBy('points', 'DESC')->count();
            $new_width1 = ($percentage1 / 100) * $totalCount1;


            $id_20 = Point::take($new_width1)->select('id')->whereNotIn('id', $id)->orderBy('points', 'DESC')->get();
            $id_array = $id->toArray();
            $id_20_array = $id_20->toArray();
            $data_id = array_merge($id_array, $id_20_array);
            $percentage2 = 15;
            $totalCount2 = Point::whereNotIn('id', $data_id)->orderBy('points', 'DESC')->count();
            $new_width2 = ($percentage2 / 100) * $totalCount2;


            $id_35 = Point::take($new_width2)->select('id')->whereNotIn('id', $data_id)->orderBy('points', 'DESC')->get();
            $id_35_array = $id_35->toArray();
            $data_id_last = array_merge($data_id, $id_35_array);
            $ranking_third_40 = Point::with('user')->whereNotIn('id', $data_id_last)->orderBy('points', 'DESC')->get();
            return response([
                'success' => true,
                'ranking_third_40' => $ranking_third_40
            ]);
        }
        return response([
            'success' => false
        ]);

    }

    /**
     * save one signal player id
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save_onesignal_player_id(Request $request)
    {
        if ($request->user()) {
            $userPlayerIds = UsersPlayerId::where('player_id', $request->player_id)->count();

            if ($userPlayerIds == 0) {

                // save the player id if not exist
                $newUserPlayerIds = new UsersPlayerId();
                $newUserPlayerIds->user_id = $request->user()->id;
                $newUserPlayerIds->player_id = $request->player_id;
                $newUserPlayerIds->save();

                return response()->json([
                    'success' => true,
                    'action' => 'save'
                ]);
            } else {

                // check user id also, update it if user has change the account on same device
                $existPlayerId = UsersPlayerId::where('player_id', $request->player_id)->first();
                if (!empty($existPlayerId)) {
                    $updatePlayerId = UsersPlayerId::find($existPlayerId->id);
                    $updatePlayerId->user_id = $request->user()->id;
                    $updatePlayerId->save();
                }

                return response()->json([
                    'success' => true,
                    'action' => 'already saved'
                ]);
            }
        }

        return response()->json([
            'success' => false
        ]);
    }

    /**
     * open video page
     */
    public function add_video($challenge_id, $user_id, $points)
    {

        return view('pages.video')->with('challenge_id', $challenge_id)->with('user_id', $user_id)->with('points', $points);
    }

    /**
     * open video page
     */
    public function save_video(Request $req)
    {
        $req->validate([
            'file' => 'mimes:mp4,mov,ogg,qt | max:10000',
            'thumbnail' => 'mimes:jpeg,jpg,png,gif|required|max:10000' // max 10000kb
        ]);
        $game_entire_video_count = game_entire_video::where('challenge_id', $req->challege_id)->count();
        if ($game_entire_video_count % 20 == 0) {
            $k = $game_entire_video_count / 20;
        }
        $fileModel = new game_entire_video;
        if ($req->hasFile('thumbnail')) {
            $image    = $req->file('thumbnail');
            $fileName = 'video_challenge' . '_' . time() . '.' . $image->getClientOriginalExtension();
            Storage::disk('uploads')->put('images/video_challenge/' . $fileName, File($image));
            $filepath        = 'uploads/images/video_challenge/' . $fileName;

            $fileModel->thumbnail = $filepath;
        }else{
            $fileModel->thumbnail = $this->http . 'image/default-video-thumbnail.jpg';
        }
        if ($req->file()) {
            $file = $req->file;
            $fileName = time() . '_' . $req->file->getClientOriginalName();
//            $filePath = $req->file('file')->storeAs('uploads', $fileName, 'public');

            $path = public_path() . '/uploads/';
            $file->move($path, $fileName);


            $fileModel->name = time() . '_' . $req->file->getClientOriginalName();
            $fileModel->file_path = $fileName;
            $fileModel->challenge_id = $req->challege_id;
            $fileModel->user_id = $req->user_id;
            $fileModel->description = $req->description;
            if (isset($k)) {
                $fileModel->game_group = 'group_of_people' . $k . '_' . $req->challege_id;
            } else {
                $last_entry = DB::table('game_entire_videos')->where('challenge_id', $req->challege_id)->latest('updated_at')->first();
                $fileModel->game_group = $last_entry->game_group;
            }
//            $fileModel->game_group = '1';
            $fileModel->save();
            Helper::save_points($req->user_id, $req->points);
            DB::table('mygames')->where('user_id', $req->user_id)
                ->where('challenge_id', $req->challege_id)
                ->update(['status' => 'done']);
            return back()
                ->with('success', 'Video has been uploaded. Please Close the Window')
                ->with('file', $fileName);
        }
    }


    public function get_video_challenge_data(Request $request)
    {

        $challenge = array();
        $challenge = DB::table('challenges')->where('active', 1)->where('id', $request->challenge_id_group)->get();

        if ($challenge) {
            foreach ($challenge as $key => $challenges) {
                $title = $challenges->title;
                $description = $challenges->description;
                $hashtag = $challenges->hashtag;
                $id = $challenges->id;
                $image = $this->http . $challenges->image;
                $points = $challenges->points;
                $publish_by = $challenges->publish_by;
                $thumbnail = $this->http . $challenges->thumbnail;


                $challenge = array(
                    "img" => $image,
                    "title" => $title,
                    "description" => $description,
                    'hashtag' => $hashtag,
                    "id" => $id,
                    "points" => $points,
                    'publish_by' => $publish_by,
                    'thumbnail' => $thumbnail
                );
            }
            $group_entry_id = "";
            $group_entry_d = game_entire_video::where('challenge_id', $request->challenge_id_group)->where('user_id', $request->user_id)->first();
            if (isset($group_entry_d) && $group_entry_d != null) {
                $group_entry_id = $group_entry_d->id;
            }

            return response([
                'success' => true,
                'challenge' => $challenge,
                'group_entry_id' => $group_entry_id,
                'thumbnail' => $this->http . 'image/default-video-thumbnail.jpg',
            ]);
        }

        return response([
            'success' => false
        ]);

    }

    public function get_all_video_entry(Request $request)
    {
        $group_entry_id = "";
        $id_game = null;
        $group_entry_all_data = array();
        $group_entry_latest = array();
        if (isset($request->challenge_id_group) && $request->challenge_id_group != NULL) {
//            $group_entry_top = game_entire_video::with('user')->where('challenge_id', $request->challenge_id_group)->inRandomOrder()->get();
            $group_entry_latest_array = game_entire_video::with('user')->where('challenge_id', $request->challenge_id_group)->inRandomOrder()->get();
            $key = 0;
//            foreach ($group_entry_top as $value) {
//                $GameEntryLikeData = Helper::get_like_count_data($value->id);
//
//                if ($GameEntryLikeData > 10) {
//                    $group_entry_all_data[$key] = array(
//                        "GameEntryLikeData" => $GameEntryLikeData,
//                        "challenge_id" => $value->challenge_id,
//                        "active_id" => $value->active_id,
//                        'description' => $value->description,
//                        "game_group" => $value->game_group,
//                        "id" => $value->id,
//                        'image' => $value->image,
//                        'report_id' => $value->report_id,
//                        'user_id' => $value->user_id,
//                        'group_entry' => $value,
////                        'user_data' => Helper::get_user_data($value->user_id)
//                    );
//                    $key++;
//                }
//            }
            $key1 = 0;
            foreach ($group_entry_latest_array as $value) {
                $GameEntryLikeData1 = Helper::get_like_count_data($value->id, $value->challenge_id, $value->user_id);
                $GameEntryLikeDataIcon = Helper::get_like_count_data_icon($value->id, $value->challenge_id, $request->user_id);

                $GameEntryCommentData = Helper::get_comment_count_data($value->id, $value->challenge_id, $value->user_id);
                if ($GameEntryLikeData1 > 10) {
                    $group_entry_latest[$key1] = array(
                        "GameEntryLikeData" => $GameEntryLikeData1,
                        "GameEntryCommentData" => $GameEntryCommentData,
                        "GameEntryLikeDataIcon" => $GameEntryLikeDataIcon === 0 ? "outline" : "sharp",
                        "challenge_id" => $value->challenge_id,
                        "active_id" => $value->active_id,
                        'description' => $value->description,
                        "game_group" => $value->game_group,
                        "id" => $value->id,
                        'image' => $this->http . '/uploads/' . $value->file_path,
                        'report_id' => $value->report_id,
                        'user_id' => $value->user_id,
                        'group_entry' => $value,
//                    'user_data' => Helper::get_user_data($value->user_id)
                    );
                    $key1++;
                }
            }

        }
        return response([
            'success' => true,
            'group_entry_all_data' => $group_entry_all_data,
//            'latest_entry' => $group_entry_latest
        ]);
    }

    /**
     * get all the group entry Video
     */
    public function get_all_group_entry_video(Request $request)
    {

        $get_group_entry = game_entire_video::where('id', $request->group_game_id)->first();
        $GameEntryLikeCount = array();
        if (isset($get_group_entry) && $get_group_entry != null) {
            $get_all_group_entry = game_entire_video::with('user')->where('game_group', $get_group_entry->game_group)->get();

            foreach ($get_all_group_entry as $key => $value) {
                $GameEntryLikeData = Helper::get_like_count_data($value->id, $value->challenge_id, $value->user_id);
                $GameEntryLikeDataIcon = Helper::get_like_count_data_icon($value->id, $value->challenge_id, $request->user_id);
                $GameEntryCommentData = Helper::get_comment_count_data($value->id, $value->challenge_id, $value->user_id);
                $GameEntryLikeCount[$key] = array(
                    "GameEntryLikeData" => $GameEntryLikeData,
                    "GameEntryCommentData" => $GameEntryCommentData,
                    "GameEntryLikeDataIcon" => $GameEntryLikeDataIcon === 0 ? "outline" : "sharp",
                    "challenge_id" => $value->challenge_id,
                    "active_id" => $value->active_id,
                    'description' => $value->description,
                    "game_group" => $value->game_group,
                    "id" => $value->id,
                    'image' => $this->http . '/uploads/' . $value->file_path,
                    'report_id' => $value->report_id,
                    'user_id' => $value->user_id,
                    'group_entry' => $value,
//                    'user_data' => Helper::get_user_data($value->user_id)
                );


            }

            return response([
                'success' => true,
                'all_group_entry' => $GameEntryLikeCount,
                'game_group' => $get_all_group_entry,
                'id' => $request->group_game_id,
                'thumbnail' => $this->http . 'image/default-video-thumbnail.jpg'
            ]);
        }
        return response([
            'success' => false,
            'get_group_entry' => $get_group_entry,
            'id' => $request->group_game_id
        ]);
    }

    public function get_user_percentage_ranking(Request $request)
    {
        if (Helper::get_5_ranking($request->user_id) == '5') {
            return response([
                'success' => true,
                'ranking' => Helper::get_5_ranking($request->user_id)
            ]);
        } elseif (Helper::get_20_ranking($request->user_id) == '20') {
            return response([
                'success' => true,
                'ranking' => Helper::get_20_ranking($request->user_id)
            ]);
        } elseif (Helper::get_35_ranking($request->user_id) == '35') {
            return response([
                'success' => true,
                'ranking' => Helper::get_35_ranking($request->user_id)
            ]);
        } elseif (Helper::get_40_ranking($request->user_id) == '40') {
            return response([
                'success' => true,
                'ranking' => Helper::get_40_ranking($request->user_id)
            ]);
        } else {
            return response([
                'success' => true,
                'ranking' => 'nothing'
            ]);
        }


    }

    public function get_user_friend_played_game(Request $request)
    {
        $get_the_entry_array = array();
        $friends = relationship::where(function ($query) use ($request) {
            $query->where('first_user_id', $request->user_id)
                ->orWhere('second_user_id', $request->user_id);
        })->where('status', 2)->get();
        foreach ($friends as $key => $friend) {
            if ($request->user_id != $friend->first_user_id) {
                $id = $friend->first_user_id;
            } else {
                $id = $friend->second_user_id;
            }
            $get_the_entry = Helper::get_the_entry($id);

            $get_the_entry_array[$key] = array(
                'get_the_entry' => $get_the_entry
            );
        }

        return response([
            'success' => true,
            'friends' => $friends,
            'get_the_entry_array' => $get_the_entry_array
        ]);
    }

    public function get_like_count_user_like_data(Request $request)
    {
        $count = Helper::get_like_count_data($request->group_entry_id_for_like, $request->challenge_id, $request->user_id);
        return response([
            'success' => true,
            'count' => $count
        ]);
    }

    public function add_challenge_from_user(Request $request){

        $challenge          = new Challenge();

        $challenge->title     = $request->title;
        $challenge->description   = $request->description;
        $challenge->hashtag = $request->hashtag;
        if($request->challenge_type == "challenge_type_image"){
            $challenge->challenge_name     = "PICTURE CHALLENGE";
            $challenge->challenge_type_video = NULL;
            $challenge->challenge_type_picture = 1;
            $challenge->challenge_type_prediction = NULL;
            if ($request->hasFile('challenge_image')) {
                $image = $request->file('challenge_image');
                $fileName = 'challenge' . '_' . time() . '.' . $image->getClientOriginalExtension();
                Storage::disk('uploads')->put('images/challenge/' . $fileName, File($image));
                $filepath = 'uploads/images/challenge/' . $fileName;

                $challenge->image = $filepath;
            }
        }else{

            $challenge->challenge_name     = "VIDEO CHALLENGE";
            $challenge->challenge_type_video = 1;
            $challenge->challenge_type_picture = NULL;
            $challenge->challenge_type_prediction = NULL;

            if ($request->hasFile('challenge_video')) {
                $image = $request->file('challenge_video');
                $fileName = 'challenge' . '_' . time() . '.' . $image->getClientOriginalExtension();
                Storage::disk('uploads')->put('images/challenge/' . $fileName, File($image));
                $filepath = 'uploads/images/challenge/' . $fileName;

                $challenge->image = $filepath;
            }

        }
        $challenge->active  = 0;
        $challenge->publish_by      = $request->user_id;
        $challenge->points      = 00;

        if ($challenge->save()) {
            return back()
                ->with('success', 'Challenge successfully added. Please Close the Window');
        }
    }

    /**
     * Check Facebook Id already exist or not
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkFbId(Request $request, $id)
    {
        if ($id) {
            $user = User::where('facebook_id', $id)->first();
            if ($user) {
                $tokenResult =   $user->createToken('authToken')->accessToken;

                $token = $tokenResult;
                $updateUser = User::find($user->id);
                $updateUser->api_token = $token;
                if ($updateUser->save()) {
                    return response()->json(['success' => true, 'token' => $token]);
                }
            }
        }

        return response()->json([
            'success' => false
        ]);
    }

    /**
     * Check checkGoogleId Id already exist or not
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkGoogleId(Request $request, $id)
    {
        if ($id) {
            $user = User::where('google_id', $id)->first();
            if ($user) {
                $tokenResult =   $user->createToken('authToken')->accessToken;

                $token = $tokenResult;
                $updateUser = User::find($user->id);
                $updateUser->api_token = $token;
                if ($updateUser->save()) {
                    return response()->json(['success' => true, 'token' => $token]);
                }
            }
        }

        return response()->json([
            'success' => false
        ]);
    }

    //open add challenge page

    public function add_challenge( $user_id)
    {
        return view('pages.add-challenges')->with('user_id', $user_id);
    }
}
