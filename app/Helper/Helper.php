<?php
//app/Helpers/helpers.php
namespace App\Helper;

use App\Models\Challenge;
use App\Models\game_entire_video;
use App\Models\game_entrie;
use App\Models\game_entry_comment;
use App\Models\GameEntryLike;
use App\Models\Point;
use App\Models\User;
use App\Models\UsersPlayerId;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Helper
{
    public static function get_like_count_data($id, $challenge_id, $user_id)
    {
        $GameEntryLikeData = GameEntryLike::where('game_entry_id', $id)
            ->where('challenge_id', $challenge_id)
            ->count();
        return $GameEntryLikeData;
    }

    public static function get_comment_count_data($id, $challenge_id, $user_id){
        $GameEntryCommentData = game_entry_comment::where('game_entry_id', $id)
            ->where('challenge_id', $challenge_id)
            ->count();
        return $GameEntryCommentData;
    }

    public static function get_like_count_data_icon($id, $challenge_id, $user_id)
    {
        $GameEntryLikeDataIcon = GameEntryLike::where('game_entry_id', $id)
            ->where('challenge_id', $challenge_id)
            ->where('user_id', $user_id)
            ->count();
        return $GameEntryLikeDataIcon;
    }

    public static function get_user_data($id)
    {
        $user = User::where('id', $id)->select('image', 'firstname', 'lastname', 'flag')->first();
        return $user;
    }

    public static function save_points($id, $p)
    {
        $points = point::where('user_id', $id)->count();
        if ($points > 0) {
            $point_exist = point::where('user_id', $id)->first();
            $total_points = $p + $point_exist->points;
            $point = DB::table('points')->where('user_id', $id)
                ->update(['points' => $total_points]);
        } else {
            $point = new point();
            $point->user_id = $id;
            $point->points = $p;
            $point->save();
        }
    }

    /**
     * Build the notification
     */
    public static function notification($user_id, $title = null, $message)
    {
        $playerIds = UsersPlayerId::select('player_id')->where('user_id', $user_id)->get();
//        echo $user_id;
//        echo $title;
//        echo $message;

        $playerIdsArray = [];
        foreach ($playerIds as $key => $playerId) {
            $playerIdsArray[$key] = $playerId->player_id;
        }

        $content = array(
            "en" => $message
        );
        $heading = array(
            "en" => "Kreelok Notification"
        );
        $fields = array(
            'app_id' => "02dc948f-5ed7-43a2-85e0-55efca9e3b29",
            //'included_segments' => array('All'),
            'include_player_ids' => $playerIdsArray, //['2e7de56b-0437-4c72-a070-af3e274312d4'],
            'data' => array(
                'title' => "message-title",
                'message' => "Go To Profile Page"
            ),
            'contents' => $content,
            'headings' => $heading
        );

        $fields = json_encode($fields);

//        print("\nJSON sent:\n");
//        print($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic ODg5ZjE4MTktYjYzNy00ZGZjLWE1MDQtNzc0N=-mE3MzdmYTM2'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $resp = curl_exec($ch);

        if ($resp === false) {
            //printf("cUrl error (#%d): %s<br>\n", curl_errno($ch), htmlspecialchars(curl_error($ch)));
        }


        $info = curl_getinfo($ch);
        curl_close($ch);
        $return["allresponses"] = $resp;
        $return = json_encode($return);

//        print("\n\nJSON received:\n");
//        print($return);
//        print("\n");

    }

    public static function get_5_ranking($user_id)
    {

        // get top 5 % data
        $percentage = 5;
        $i = "";
        $totalCount = DB::table('points')->count();
        $new_width = ($percentage / 100) * $totalCount;
        $ranking_Top_5 = Point::select('user_id')->take($new_width)->orderBy('points', 'DESC')->get()->toArray();
        $ranking_third_5_array = array();
        foreach ($ranking_Top_5 as $key => $value){
            $ranking_third_5_array[$key] = $value['user_id'];
        }

        if(!empty($ranking_third_5_array)){
            $i = in_array($user_id, $ranking_third_5_array);
        }

        if ($i) {
            $status = '5';
        } else {
            $status = '';
        }
        return $status;

    }

    public static function get_20_ranking($user_id)
    {
        $i = "";

        // get 20 % data
        $percentage = 5;
        $totalCount = DB::table('points')->count();
        $new_width = ($percentage / 100) * $totalCount;


        $id = Point::take($new_width)->select('id')->orderBy('points', 'DESC')->get();
        $percentage1 = 20;
        $totalCount1 = Point::whereNotIn('id', $id)->orderBy('points', 'DESC')->count();
        $new_width1 = ($percentage1 / 100) * $totalCount1;
        $ranking_Second_20 = Point::select('user_id')->take($new_width1)->whereNotIn('id', $id)->orderBy('points', 'DESC')->get()->toArray();

        $ranking_third_20_array = array();
        foreach ($ranking_Second_20 as $key => $value){
            $ranking_third_20_array[$key] = $value['user_id'];
        }

        if(!empty($ranking_third_20_array)){
            $i = in_array($user_id, $ranking_third_20_array);
        }

        $status = '';
        if ($i) {
            $status = '20';
        } else {
            $status = '';
        }
        return $status;
    }

    public static function get_35_ranking($user_id)
    {
        $i = "";
        // get 35 % data
        $percentage = 5;
        $totalCount = DB::table('points')->count();
        $new_width = ($percentage / 100) * $totalCount;


        $id = Point::take($new_width)->select('id')->orderBy('points', 'DESC')->get();
        $percentage1 = 20;
        $totalCount1 = Point::whereNotIn('id', $id)->orderBy('points', 'DESC')->count();
        $new_width1 = ($percentage1 / 100) * $totalCount1;


        $id_20 = Point::take($new_width1)->select('id')->whereNotIn('id', $id)->orderBy('points', 'DESC')->get();
        $id_array = $id->toArray();
        $id_20_array = $id_20->toArray();
        $data_id = array_merge($id_array, $id_20_array);
        $percentage2 = 15;
        $totalCount2 = Point::whereNotIn('id', $data_id)->orderBy('points', 'DESC')->count();
        $new_width2 = ($percentage2 / 100) * $totalCount2;
        $ranking_third_35 = Point::select('user_id')->take($new_width2)->whereNotIn('id', $data_id)->orderBy('points', 'DESC')->get()->toArray();

        $ranking_third_35_array = array();
        foreach ($ranking_third_35 as $key => $value){
            $ranking_third_35_array[$key] = $value['user_id'];
        }

        if(!empty($ranking_third_35_array)){
            $i = in_array($user_id, $ranking_third_35_array);
        }

        if ($i) {
            $status = '35';
        } else {
            $status = '';
        }
        return $status;
    }

    public static function get_40_ranking($user_id)
    {
        $i = "";
        // get 40 % data
        $percentage = 5;
        $totalCount = DB::table('points')->count();
        $new_width = ($percentage / 100) * $totalCount;


        $id = Point::take($new_width)->select('id')->orderBy('points', 'DESC')->get();
        $percentage1 = 20;
        $totalCount1 = Point::whereNotIn('id', $id)->orderBy('points', 'DESC')->count();
        $new_width1 = ($percentage1 / 100) * $totalCount1;


        $id_20 = Point::take($new_width1)->select('id')->whereNotIn('id', $id)->orderBy('points', 'DESC')->get();
        $id_array = $id->toArray();
        $id_20_array = $id_20->toArray();
        $data_id = array_merge($id_array, $id_20_array);
        $percentage2 = 15;
        $totalCount2 = Point::whereNotIn('id', $data_id)->orderBy('points', 'DESC')->count();
        $new_width2 = ($percentage2 / 100) * $totalCount2;


        $id_35 = Point::take($new_width2)->select('id')->whereNotIn('id', $data_id)->orderBy('points', 'DESC')->get();
        $id_35_array = $id_35->toArray();
        $data_id_last = array_merge($data_id, $id_35_array);
        $ranking_third_40 = Point::whereNotIn('id', $data_id_last)->orderBy('points', 'DESC')->get()->toArray();
        $ranking_third_40_array = array();
        foreach ($ranking_third_40 as $key => $value){
            $ranking_third_40_array[$key] = $value['user_id'];
        }

        if(!empty($ranking_third_40_array)){
            $i = in_array($user_id, $ranking_third_40_array);
        }

        if ($i) {
            $status = '40';
        } else {
            $status = '';
        }
        return $status;
    }

    public static function get_the_entry($id)
    {
//        $http = 'http://playbox.webronix.com/';
        $http = 'https://kreelok.com/';
        $game_entrie = game_entrie::where('user_id', $id)->first();
        $game_entire_video = game_entire_video::where('user_id', $id)->first();
        if(!empty($game_entrie)){
            $data_id = $game_entrie->challenge_id;
            $data['challenge'] = DB::table('challenges')->where('active', 1)
                ->where('id', $data_id)->first();
            $data['game_entrie'] = $game_entrie->image;
            $data['user'] =  User::where('id', $id)->first();
//            $data = array_push($challenge, $user->firstname);
        }
        if(!empty($game_entire_video)){
            $data_id = $game_entire_video->challenge_id;
            $data['challenge'] = DB::table('challenges')->where('active', 1)
                ->where('id', $data_id)->first();
            $data['user'] =  User::where('id', $id)->first();
            $data['game_entrie'] = array([
                'file_path' => $http . '/uploads/' . $game_entire_video->file_path,
                'thumbnail' => $http . '/' . $game_entire_video->thumbnail

            ]);
//            $data = array_push($challenge, $user->firstname);
//            $data = $challenge;
        }

        return $data;
    }

    public static function report_after_10_people($user_id,$challenge_id,$game_entry_id,$challenge_name){

        $Challenge = Challenge::select('points')->where('id', $challenge_id)->first();

        $points = point::where('user_id', $user_id)->first();

        if(isset($Challenge) && $Challenge != null && isset($points) && $points != null){
            if($points->points != 0){
                $points_now = $points->points - $Challenge->points;
                $point = DB::table('points')->where('user_id', $user_id)
                    ->update(['points' => $points_now]);

            }
            if($challenge_name === "picture"){
//                Helper::notification($user_id, "null", "Your post is blocked now");
                DB::table('game_entries')->where('id', $game_entry_id)->where('challenge_id', $challenge_id)
                    ->update(['report_id' => 1]);

            }else{
//                Helper::notification($user_id, "null", "Your Video is blocked now");
                DB::table('game_entire_videos')->where('id', $game_entry_id)->where('challenge_id', $challenge_id)
                    ->update(['report_id' => 1]);
            }
            return $point;
        }


    }

}
