<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class game_entrie extends Model
{
    use HasFactory;

    protected $table = 'game_entries';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id'
    ];
    public function user() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function GameEntryLike() {
        return $this->hasOne(GameEntryLike::class, 'id', 'game_entry_id');
    }
}
