<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class game_entry_comment extends Model
{
    use HasFactory;
    protected $table = 'game_entry_comments';
}
